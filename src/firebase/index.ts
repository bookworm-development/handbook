import * as fb from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import { configuration } from './configuration';

// Initialize the firebase application
export const firebaseApp = fb.initializeApp(configuration, 'Handbook');

firebaseApp.auth().useDeviceLanguage();

// Initialize the firestore database
export const firebaseDB = firebaseApp.firestore();

export * from './firebaseDB';
