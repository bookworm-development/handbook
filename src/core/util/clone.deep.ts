export function clone(obj: any): any {
  return Array.isArray(obj)
    ? obj.map((item) => clone(item))
    : obj instanceof Date
    ? new Date(obj.getTime())
    : obj && typeof obj === 'object'
    ? Object.getOwnPropertyNames(obj).reduce((o, prop) => {
        if (obj[prop] && obj[prop].get && obj[prop].get.call) {
          // Ignore proxyed DocumentReference's
          (o as { [p: string]: any })[prop] = null;
        } else {
          (o as { [p: string]: any })[prop] = clone(obj[prop]);
        }

        return o;
      }, {})
    : obj;
}
