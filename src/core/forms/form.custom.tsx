import { runInAction } from '../store';
import { FormStore } from '../store/form';
import { action, Buttons, computed, ElementChild, ElementComponent, Form, h, IFormProps } from '../ui/src';
import { IButtonProps } from '../ui/src/button/button';

export interface ICustomFormProps<T> extends IFormProps {
  actionTraps?: ICustomFormActionTraps;
  store?: FormStore<T>;
  overwrite?: FormStore<T>;
}

export interface ICustomFormActionTraps {
  submitBtn?: ICustomFormActionTrap;
  resetBtn?: ICustomFormActionTrap;
}

export interface ICustomFormActionTrap {
  after?: ValueTrap;
  before?: ActionTrap;
}

export type ActionTrap = (ev: MouseEvent) => void;
export type ValueTrap = (v: any) => void;

export interface ICustomFormActions {
  group?: boolean; // Determines if the buttons will be grouped (defaults to true)
  submitBtn?: IButtonProps | null;
  resetBtn?: IButtonProps | null;
  extra?: IButtonProps[]; // Extra buttons to be added (can replace the submit and reset, if ommited)
  wrapActions?: boolean;
}

export abstract class CustomForm<F, P extends ICustomFormProps<F>, S> extends ElementComponent<P, S> {
  protected baseClass: string = '';

  public render() {
    return <Form.Form {...this.props}>
      {this.fields}
      {this.actionElements}
    </Form.Form>;
  }

  protected abstract get actions(): ICustomFormActions;

  protected abstract get fields(): ElementChild;

  protected missingSubmitFn() {
    throw new Error('Missing submit button function.');
  }

  @computed
  protected get store(): FormStore<F> {
    if (!this.props.store && !this.props.overwrite) {
      throw new Error('Custom form missing form store.');
    }

    return (this.props.overwrite || this.props.store) as FormStore<F>;
  }

  @computed
  protected get actionElements() {
    // Enforce defaults on missing button values
    const actions: ICustomFormActions = {
      ...this.actions || {},
    };

    actions.group = actions.group || true;
    actions.extra = actions.extra || [];

    if (!actions.extra.length) {
      if (actions.submitBtn !== null) {
        actions.extra.unshift({
          disabled:  !this.store.submitAction,
          handler: this.missingSubmitFn,
          label: 'Submit',
          styleType: 'primary',
          type: 'submit',
          width: '3-5',
          // Override defaults with provided configuration
          ...(actions.submitBtn || {}),
        });

        actions.extra[0].handler = this.factoryActionTraps(
          actions.extra[0].handler,
          ((this.props.actionTraps || {}) as ICustomFormActionTraps).submitBtn || {},
        );
      }

      if (actions.resetBtn !== null) {
        actions.extra.unshift({
          disabled:  !this.store.resetAction,
          handler: this.store.reset,
          label: 'Reset',
          styleType: 'secondary',
          type: 'reset',
          width: '2-5',
          // Override defaults with provided configuration
          ...(actions.resetBtn || {}),
        });

        actions.extra[0].handler = this.factoryActionTraps(
          actions.extra[0].handler,
          ((this.props.actionTraps || {}) as ICustomFormActionTraps).resetBtn || {},
        );
      }
    }

    const buttons = actions.group ?
      <Buttons.ButtonGroup
        margin={'small-top'}
        width={'1-1'}
        buttons={actions.extra}/> :
      actions.extra.map((b) => <Buttons.Button {...b} />);

    return actions.wrapActions ? <Form.Field>
      { buttons }
      </Form.Field> :
      buttons;
  }

  private factoryActionTraps(handler: IButtonProps['handler'], traps: ICustomFormActionTrap) {
    const before = traps.before ? traps.before : this.noOpBeforeAction;
    const after = traps.after ? traps.after : this.noOpAfterAction;

    return action(async (ev: MouseEvent) => {
      runInAction(() => before(ev));
      const val = await handler(ev);
      runInAction(() => after(val));
    });
  }

  private noOpBeforeAction(_: MouseEvent) {
    // NoOp
  }

  private noOpAfterAction(_: MouseEvent) {
    // NoOp
  }
}
