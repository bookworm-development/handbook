import { Component, h } from 'preact';
import * as UIKit from 'uikit';

import { computed, Containers, Icons, observer, ElementChild, action } from '../../ui/src';
import { ISelectProps } from '../../ui/src/form/select/select';

import './select.scss';
import { Omit } from '../../chartjs-preact/src/omit';

interface IComplexSelectProps extends Omit<ISelectProps, 'options'> {
  placeholder?: string;
  options: IOption[];
}

interface IOption {
  value: string;
  label: ElementChild;
  categories?: IOptionCategory[] | string[];
}

interface IOptionCategory {
  value: string;
  label: string;
}

@observer
export class ComplexSelect extends Component<IComplexSelectProps, any> {
  private dropdownRef: HTMLElement;

  public render() {
    return <div>
      <input className='uk-select' value={this.props.value.get() as any} placeholder={this.props.placeholder} />
      <Containers.Div
        ref={(el) => this.dropdownRef = el}
        dropDown={{ pos: 'bottom-justify', offset: 3, mode: 'click' }} onClickCapture={this.selectionChange}
        {...this.attributes} class='ntbk-select' padding='remove' margin='remove'>
        <ul className='uk-list uk-margin-remove'>{ this.items }</ul>
      </Containers.Div>
    </div>;
  }

  @action.bound
  private selectionChange(ev: MouseEvent) {
    const listItem = this.findParent(ev.target as HTMLElement, 'li');
    const itemId = listItem.getAttribute('data-value');

    this.props.value.set(itemId as string);

    UIKit.dropdown((this.dropdownRef as unknown as { base: HTMLElement }).base).hide(true);
  }

  @computed
  private get attributes() {
    return {
      ...this.props.inverse ? { inverse: this.props.inverse } : {},
    };
  }

  @computed
  private get items() {
    return this.props.options.map((o) => {
      return <li data-value={o.value} className='uk-margin-remove'>
        <Containers.Div flex={['1', 'middle']} userSelect='none'>
          {
            this.props.value.get() === o.value ?
              <Icons.Icon icon={Icons.app.check} /> :
              <span className='uk-icon empty' />
          }
          {o.label}
        </Containers.Div>
      </li>;
    });
  }

  private findParent(element: HTMLElement, tagName: string) {
    let el = element;
    const t = tagName.toUpperCase();

    while (el) {
      if (el.tagName === t) {
        return el;
      }

      el = el.parentElement as HTMLElement;
    }

    return el;
  }
}
