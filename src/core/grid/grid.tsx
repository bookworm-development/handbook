import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-preact';
import { Component, h } from 'preact';

import { delayInAction } from '../delayers';
import { Containers, ElementChild, Icons, Link, Tables } from '../ui/src';
import { ITableProps } from '../ui/src/tables/table';
import { ITableCellProps } from '../ui/src/tables/table.cell';

import './grid.scss';

interface IGridProps<T> {
  caption?: ElementChild;
  empty?: ElementChild;
  filter?: Array<Array<ICategoryFilterRules<T>>>;
  footer?: ElementChild;
  categories: Array<ICategorySetting<T>>;
  selectable?: boolean;
  onSelection?: (item: T) => void;
  sort?: Array<ICategorySortRules<T>>;
  tableOptions?: ITableProps;
  data: T[];
}

export interface ICategorySetting<T> {
  field: keyof T;
  options?: ITableCellProps;
  label: ElementChild;
  render: FieldRenderer<T>;
}

export interface ICategorySortRules<T> {
  field: keyof T;
  order: 'asc' | 'desc' | null;
  valueFn?: (rawValue: any) => any;
}

export interface ICategoryFilterRules<T> {
  field: keyof T;
  term: string;
  filter?: (value: any, term: string) => boolean;
}

type FieldRenderer<T> = (v: any, key: keyof T) => ElementChild;

@observer
export class DataGrid<T> extends Component<IGridProps<T>, any> {
  public static RenderFieldAsString: FieldRenderer<any> = (v: any, _: any) => (v || '').toString();
  private static SelectedRow = 'selected';

  private fieldsConfiguration: { [field in keyof T]: ICategorySetting<T>; };

  private previouslySelected: HTMLElement | null;

  @observable
  private rules: {
      sorting: { [field in keyof T]: ICategorySortRules<T>['order'] };
      filtering: { [field in keyof T]: ICategoryFilterRules<T>['filter'] } } =
    { sorting: {} as any, filtering: {} as any };

  public componentWillUnmount() {
    // Make sure to not polute the next grid with the current's selection
    if (this.previouslySelected) {
      this.previouslySelected.classList.remove(DataGrid.SelectedRow);
    }
  }

  public render() {
    return <Tables.Table {...this.props.tableOptions || {}}>
      {this.caption}
      {this.header}
      {this.footer}
      {this.body}
    </Tables.Table>;
  }

  @computed
  protected get caption() {
    return this.props.caption ?
      <Tables.Caption>{this.props.caption}</Tables.Caption> :
      null;
  }

  @computed
  protected get header() {
    const sortingRules = this.sortingRules;

    return <Tables.Header>
      <Tables.Row>
        {
          this.props.categories.map((c) => {
            return <Tables.Cell heading {...c.options || {}}>
              <Containers.Div flex='middle' cursor='default' userSelect='none'>
                {c.label}
                {
                  sortingRules[c.field] !== undefined ?
                    <Link
                      label={
                        <Icons.Icon icon={
                          !sortingRules[c.field] ?
                            // Undetermined sorting direction
                            Icons.direction.shrink :
                            sortingRules[c.field] === 'asc' ?
                            // Ascendent sorting
                            Icons.direction['chevron-up'] :
                            Icons.direction['chevron-down']
                        } margin='small-left' />
                      }
                      handler={(ev) => { ev.preventDefault(); this.changeSortOrder(c.field); }} /> :
                    null
                }
              </Containers.Div>
            </Tables.Cell>;
          })
        }
      </Tables.Row>
    </Tables.Header>;
  }

  @computed
  protected get sortingRules() {
    const sortingRules: { [field in keyof T]: ICategorySortRules<T>['order'] } = {} as any;
    (this.props.sort || []).forEach((r) =>  sortingRules[r.field] = r.order);

    return {
      ...sortingRules,
      ...this.rules.sorting, // Preffer internal sorting state
    };
  }

  @action
  protected changeSortOrder(field: keyof T) {
    const initialData = this.data;

    // Enforce the currently provided values
    this.rules.sorting[field] = this.sortingRules[field] || null;

    const initialVal = this.rules.sorting[field];
    const endVal = !this.rules.sorting[field] ? 'asc' :
      this.rules.sorting[field] === 'asc' ? 'desc' :
      'asc';

    // Toggle between asc and desc
    this.rules.sorting[field] = endVal;

    const changedData = this.data;

    // Revert changes
    this.rules.sorting[field] = initialVal;

    const toHide: number[] = [];

    changedData.forEach((d, i) => {
      if (d !== initialData[i]) {
        toHide.push(i);
      }
    });

    const table =  this.base as HTMLTableElement;
    const rows = table.tBodies[0].rows;

    delayInAction(() => {
      this.rules.sorting[field] = endVal;

      toHide.forEach((i) => {
        rows[i].classList.add('flash');

        const handle = () => {
          rows[i].removeEventListener('animationend', handle);

          rows[i].classList.remove('flash');
        };

        rows[i].addEventListener('animationend', handle);
      });
    }, 0);
  }

  @computed
  protected get footer() {
    return this.props.footer ?
      <Tables.Footer>{this.props.footer}</Tables.Footer> :
      null;
  }

  @computed
  protected get body() {
    return <Tables.Body>
      {
        this.data.length ?
          this.data.map((r, i) => {
            return <Tables.Row>
              {
                this.fields.map((f) => this.renderField(f, r, i))
              }
            </Tables.Row>;
          }) :
          this.empty
      }
    </Tables.Body>;
  }

  @computed
  protected get empty() {
    return <Tables.Row>
      <Tables.Cell colspan={this.props.categories.length}>
        <Containers.Div flex={['1', 'middle', 'center']} style={{ minHeight: '69.8vh' }}>
          { this.props.empty }
        </Containers.Div>
      </Tables.Cell>
    </Tables.Row>;
  }

  @computed
  private get data(): T[] {
    let data: T[] = [
      ...this.props.data,
    ];

    if (this.props.filter && this.props.filter.length) {
      // Filter data before sorting
      data = data.filter((d) => {
        let matchedOne = false;

        for (const andFilters of (this.props.filter as Array<Array<ICategoryFilterRules<T>>>)) {
          for (const f of andFilters) {
            // Ignore empty filters and missing fields
            if (f.term && (f.field in d)) {
              if (f.filter) {
                if (f.filter(d[f.field], f.term)) {
                  matchedOne = true;
                  break; // Stop early
                }
              } else {
                if (('' + d[f.field]).toLowerCase().indexOf(f.term.toLowerCase()) > -1) {
                  matchedOne = true;
                  break; // Stop early
                }
              }
            }
          }

          if (matchedOne) {
            break;
          }
        }

        return matchedOne;
      });
    }

    if (this.props.sort) {
      const currentSortingRules = this.sortingRules;
      const sortingRules = this.props.sort.filter((r) => r.order).map((r) => {
        r.order = currentSortingRules[r.field] !== undefined ? currentSortingRules[r.field] : r.order;
        return r;
      });

      return data.sort((a, b) => {
        return this.sortByField(a, b, 0, sortingRules);
      });
    }

    return this.props.data;
  }

  private sortByField(a: T, b: T, idx: number, fields: Array<ICategorySortRules<T>>): number {
    const rule = fields[idx];
    const field = rule.field;
    const isAsc = rule.order === 'asc';
    const relations = {
      bigger: isAsc && 1 || -1,
      equal: 0,
      smaller: isAsc && -1 || 1,
    };
    const valueOf = rule.valueFn ? rule.valueFn : (v: any) => v;

    if (field in a) {
      // Preffer existing data
      if (!(field in b)) {
        return relations.bigger;
      }

      const aa = valueOf(a[field]);
      const bb = valueOf(b[field]);

      if (aa < bb) {
        return relations.smaller;
      } else if (aa > bb) {
        return relations.bigger;
      }

      if (idx < fields.length - 1) {
        // The fields are equal based on current sorting criterias, look for the value of the next sortable field
        return this.sortByField(a, b, idx + 1, fields);
      }

      // End of sortable criterias, consider the two data sets equal
      return 0;
    }

    return this.sortByField(b, a, idx, fields) * -1;
  }

  @computed
  private get fields() {
    this.fieldsConfiguration = {} as any;
    return this.props.categories.map((c) => {
      this.fieldsConfiguration[c.field] = c;

      return c.field;
    });
  }

  private renderField(name: keyof T, data: T, _: number) {
    return <Tables.Cell forceAttributes={this.selectableFieldAttributes} userSelect='none'>
      {
        this.fieldsConfiguration[name].render ?
          this.fieldsConfiguration[name].render(data[name], name) :
          DataGrid.RenderFieldAsString(data[name], name)
      }
    </Tables.Cell>;
  }

  @computed
  private get selectableFieldAttributes() {
    return this.props.selectable ?
      {
        onClick: this.rowSelected,
      } :
      {};
  }

  @action.bound
  private rowSelected(ev: MouseEvent) {
    ev.preventDefault();
    ev.stopPropagation();

    if (this.previouslySelected) {
      this.previouslySelected.classList.remove(DataGrid.SelectedRow);
    }

    let idx = 0;
    const row = this.findParentRow(ev.target as HTMLElement);
    const tBody = this.findParentTBody(row);

    const rows = [].slice.call(tBody.rows);
    for (let i = 0; i < rows.length; i += 1) {
      const r = rows[i];

      if (r === row) {
        idx = i;

        break;
      }
    }

    const item = this.data[idx];

    this.previouslySelected = row;
    this.previouslySelected.classList.add(DataGrid.SelectedRow);

    if (this.props.onSelection) {
      this.props.onSelection(item);
    }
  }

  private findParentRow(el: HTMLElement): HTMLTableRowElement {
    if ((el as HTMLTableRowElement).cells) {
      return el as HTMLTableRowElement;
    }

    return this.findParentRow(el.parentElement as HTMLElement);
  }

  private findParentTBody(el: HTMLElement): HTMLTableElement {
    if ((el as HTMLTableElement).rows) {
      return el as HTMLTableElement;
    }

    return this.findParentTBody(el.parentElement as HTMLElement);
  }
}
