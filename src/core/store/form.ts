import { action, IComputedValue } from 'mobx';

import { delayInAction } from '../delayers';
import { IBaseFormComponent, IValidityStatus } from '../ui/src/core';
import { deepClone } from '../util';
import { computed, observable, runInAction } from './index';
import { Store } from './store';
import { IValidationConfiguration, ValidationRules, Validators } from './validation';

interface IFormStoreAncestorPropagation<T> {
  parent: FormStore<T>;
  path: string | string[];
}

export abstract class FormStore<FormFields extends { [prop: string]: any; }> extends Store {
  [formField: string]: IComputedValue<any> | any; // Genericly accept all form fields

  protected abstract validationRules: IFieldsValidationRules;

  @observable
  protected values: IFormValues<FormFields>;

  @observable protected invalidFields: number;
  @observable protected touched: boolean;

  // TransformersIn allow transformation of raw input values (type casting most of the time)
  protected transformersIn: { [f in keyof FormFields]?: ITransformerIn };

  // TransformersOut allow transformation of field output values (if fancy data modeling is required for ease of use)
  protected transformersOut: { [f in keyof FormFields]?: TransformersOut };

  // Reset values that can be used in child class reset
  protected initialValues: ResetValues<FormFields>;

  // Acestor form store propagation mechanism
  protected propagation: IFormStoreAncestorPropagation<any>;

  // Field value determination progress
  private fieldProgress: { [formField: string]: boolean } = {};

  private embeddedFieldsCache: { [path: string]: IAnyHash; } = {};

  public constructor(values?: ResetValues<FormFields>, propagate?: IFormStoreAncestorPropagation<any>) {
    super();

    if (values) {
      this.initialValues = values;
    }

    if (propagate) {
      this.propagation = propagate;
    }

    // Reset and initialize the form fields
    this.reset();

    // Create automatic getters/setters for each field
    Object.keys(this.values).forEach((field) => {
      this[field] = computed(
        // Getter
        () => this.transformersOut && field in this.transformersOut ?
          (this.transformersOut[field] as TransformersOut)(
            this.values[field].value) : // Transform the value if required
          this.values[field].value,
        // Setter
        async (val) => {
          let value: any;
          if (this.transformersIn && field in this.transformersIn) {
            const transformation = this.transformersIn[field] as ITransformerIn;

            try {
              this.fieldProgress[field] = true;
              (transformation.affected || []).forEach((f) => {
                this.fieldProgress[f] = true;
              });

              // Wait for the current field's transformation
              value = await transformation.transformation(val);

              runInAction(async () => {
                await Promise.all(transformation.affected.map((f) => {
                  return (this.transformersIn[f] as ITransformerIn).transformation(this.values[f].value);
                }));

                runInAction(() => {
                  // Mark the field as updated and set it's new value
                  this.fieldProgress[field] = false;
                });
              });
            } catch (_) {
              // Add a custom error to denote that an internal operation has failed
              runInAction(() => {
                this.values[field].validity = {
                  reason: 'error',
                  valid: false,
                };
              });
            }
          } else {
            value = val;
          }

          // Update the field's value taking into account validation
          runInAction(() => this.updateField(field, value));
        },
      );
    });
  }

  @computed
  public get value() {
    return Object.keys(this.values).reduce((obj, k) => {
      obj[k] = deepClone(this.values[k].value);

      return obj;
    }, {} as FormFields);
  }

  @action.bound
  public reset() {
    // Reset the embedded fields cache
    this.embeddedFieldsCache = {};

    // Reset the fields (most forms will define their values only at this stage)
    this.resetFields();

    // And keep track of touch and invalidity status
    delayInAction(() => {
      // Mark all mandatory fields as invalid
      const requiredFields = Object.keys(this.validationRules);
      this.invalidFields = Object.keys(requiredFields).length;

      // Check for actual invalidity
      requiredFields.forEach((f) => this.validateField(f, this.values[f].value));
    });
    // setTimeout();
    this.touched = false;
  }

  public abstract resetFields(): void;

  // Checks if the reset action should be available in the context of a form
  // (if any of the fields has been at least touched)
  @computed
  public get resetAction() {
    return this.touched;
  }

  // Checks if the submit action should be available in the context of a form
  // (if all fields are valid and touched)
  @computed
  public get submitAction() {
    return this.invalidFields === 0;
  }

  /**
   * Generic update field mechanism (allows binding to UIKit components from uikit-preact)
   */
  @action
  public updateField(name: string, value: FormPrimitiveValue) {
    if (!name) {
      throw new Error('Missing field name.');
    }

    // Validate the field before assignement
    if (this.validationRules[name]) {
      const { reason, valid } = this.validateField(name, value);

      // Update field's validity
      this.values[name].validity = {
        reason,
        valid,
      };
    }

    // Update the field's value if valid
    this.values[name].value = value;

    // Mark the form as touched
    this.touched = true;

    // Announce the parent form of changes
    if (this.propagation) {
      // Allow propagation of changes in the parent form for direct fields or embedded fields
      delayInAction(() => {
        if (typeof this.propagation.path === 'string') {
          this.propagation.parent.fieldMeta(this.propagation.path).value.set(this.value);
        } else {
          // Embedded
          this.propagation.parent.embededFieldMeta(this.propagation.path).value.set(this.value);
        }
      });
    }
  }

  /**
   * Pregenerates an object containing most common form components attributes
   */
  public fieldMeta(fieldName: keyof FormFields): {
    loading: boolean;
    name: string;
    validity?: IBaseFormComponent['validity'];
    value: any;
  } {
    const field = this.values[fieldName];
    const validity = field.validity ?
      {
        reason: (field.validity as IValidityStatus).reason,
        valid: (field.validity as IValidityStatus).valid,
      } :
      null;

    return {
      loading: field.loading,
      name: fieldName as string,
      value: this[fieldName as string] as IComputedValue<any>,
      ...validity ? { validity } : {},
    };
  }

  public embededFieldMeta(fieldPath: string[]): {
    loading: boolean;
    name: string;
    validity?: IBaseFormComponent['validity'];
    value: any;
  } {
    const fieldName = fieldPath[0] as keyof FormFields;
    const field = this.values[fieldName];

    const value = computed(
      // Getter
      () =>
        this.getEmbeddedFieldContainer(field, fieldPath.slice(1, -1), fieldName)[fieldPath[fieldPath.length - 1]],
      // Setter
      (val: any) => {
        this.getEmbeddedFieldContainer(field, fieldPath.slice(1, -1), fieldName)[fieldPath[fieldPath.length - 1]] = val;
        this.updateField(fieldName as string, field.value);
      },
    );

    return {
      loading: field.loading,
      name: fieldPath.join('.'),
      value,
    };
  }

  @action
  protected mergeInitialValues() {
    if (this.initialValues) {
      Object.keys(this.initialValues).forEach((k) => {
        this.values[k].value = this.initialValues[k];
      });
    }
  }

  // Validates a field provided it's name
  private validateField(name: string, value: FormPrimitiveValue) {
    let valid = true;
    let reason = '';

    const validationField = this.validationRules[name];
    const rules = Object.keys(validationField) as ValidationRules[];

    for (const rule of rules) {
      if (!Validators[rule](value, validationField[rule])) {
        valid = false;
        reason = rule;
        break;
      }
    }

    // Increment the invalidity counter on fields becoming invalid
    if (this.values[name].validity && (this.values[name].validity as IValidityStatus).valid && !valid) {
      this.invalidFields += 1;
    } else if (
      (
        !this.values[name].validity || // No known validity status (first time touched)
        !(this.values[name].validity as IValidityStatus).valid // Or previously invalid
      ) && valid
    ) {
      // Decrement the invalidit counter on fields becoming valid
      this.invalidFields -= 1;
    }

    return {
      reason,
      valid,
    };
  }

  private getEmbeddedFieldContainer(field: FormFieldTypes<any>, path: string[], fieldName: keyof FormFields): IAnyHash {
    const stringPath = [fieldName as string, ...path].join('.');

    if (this.embeddedFieldsCache[stringPath]) {
      return this.embeddedFieldsCache[stringPath];
    }

    let obj = field.value as IAnyHash;

    for (const p of path) {
      if (p in obj) {
        obj = obj[p] as IAnyHash;
      }
    }

    // Cache for later lookups
    this.embeddedFieldsCache[stringPath] = obj;

    return obj;
  }
}

interface IAnyHash { [p: string]: any; }

type ResetValues<FormFields extends { [prop: string]: any; }> = {
  [p in keyof FormFields]: FormFields[p]['value'];
};

export type IFormValues<T> = {
  [prop in keyof T]: FormFieldTypes<T[prop]>;
};

export type FormPrimitiveValue = string | boolean | number | null | Array<string | boolean | number | null>;

interface ITransformerIn {
  // Affected fields from current transformation,
  // at the end of the current field's transformation all affected fields transformations will be triggered
  affected: string[];
  transformation: (val: FormPrimitiveValue) => Promise<FormPrimitiveValue> | FormPrimitiveValue | any;
}

type TransformersOut = (val: FormPrimitiveValue) => FormPrimitiveValue;

type FormFieldTypes<T> = IFieldSelect<T> | IFieldCheckboxLike<T> | IFieldInputLike<T> | IFieldNumericInputLike<T>;

interface IField<T> {
  loading: boolean;
  value: T;
  validity?: IValidityStatus;
}

interface IFieldSelect<T> extends IField<T> {
  options: ISelectOption[];
}

interface IFieldCheckboxLike<T> extends IField<T> {
  value: T;
}

interface IFieldInputLike<T> extends IField<T> {
  value: T;
}

interface IFieldNumericInputLike<T> extends IField<T> {
  value: T;
}

interface ISelectOption {
  key: string;
  value: FormPrimitiveValue;
  label: string;
}

type FieldValidationRules = {
  [rule in ValidationRules]?: IValidationConfiguration[rule];
};

export interface IFieldsValidationRules { [prop: string]: FieldValidationRules; }
