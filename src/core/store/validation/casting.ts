export type CastingRules = 'number' | 'boolean' | 'string' | 'none';

export const Castings: AllCastings = {
  boolean: (value) => {
    const lookup = booleanMap[value];
    if (lookup !== undefined) {
      return lookup;
    }

    if (typeof value === 'string') {
      const stringLookup = booleanMap[value.toLowerCase().trim()];

      if (stringLookup !== undefined) {
        return stringLookup;
      }
    }

    return !!value;
  },
  none: (value) => value,
  number: (value) => {
    if (typeof value === 'string') {
      const v = value.toLowerCase().trim();
      const int = parseInt(v, 10);
      const float = parseFloat(v);

      const isInt = !isNaN(int);
      const isFloat = !isNaN(float);

      if (isInt && isFloat) {
        if (int === float) {
          return int;
        } else {
          return float;
        }
      } else if (isInt) {
        return int;
      } else if (isFloat) {
        return float;
      }
    } else if (typeof value === 'number') {
      return value;
    }

    return NaN;
  },
  string: (value) => (value).toString(),
};

const booleanMap: { [p: string]: boolean; } = {
  FALSE: false,
  TRUE: true,

  false: false,
  true: true,

  0: false,
  1: true,
};

type AllCastings = {
  [ cast in CastingRules ]: (value: any) => any;
};
