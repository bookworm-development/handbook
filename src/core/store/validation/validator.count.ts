import { FormPrimitiveValue } from '../form';
import { Validator } from './validator';

export const countValidator: Validator = (
  value: FormPrimitiveValue,
  configuration?: ICountConfiguration) => {
  const v: any[] = value && (value as any[]).map ? value as any[] : [value];
  const c = { min: -Infinity, max: Infinity, ...configuration };

  return v.length >= c.min && c.max >= v.length;
};

export interface ICountConfiguration {
  min?: number;
  max?: number;
}
