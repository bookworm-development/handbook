import { delayInAction } from '../delayers';
import { action, computed, observable, runInAction, Store} from '../store';

interface IDefinition {
  [p: string]: IDefinition | string | number;
  [n: number]: IDefinition | string | number;
}

export class I18nStore extends Store {
  public static DefaultLocale = 'ro';

  @observable
  private innerLocale: string;

  private internalScope: string = 'application';

  private definitions: { [locale: string]: IDefinition; } = {};

  @observable
  private isLoading: boolean = false;

  public constructor() {
    super();

    // Fetch the default locale resources
    this.change(I18nStore.DefaultLocale);
  }

  public reset() {
    this.innerLocale = I18nStore.DefaultLocale;
    this.isLoading = false;
  }

  // At the moment only allows grabbing the rw text from definitions (no interpolation what so ever)
  public translate(
    id: string,
    fields?: { [p: string]: any },
    plural?: number,
    fallback?: string) {
    let raw = this.getRawText(
      this.scope ? this.definition[this.scope] as IDefinition : this.definition, id.split('.'), 0) || fallback || '';

    if (fields) {
      Object.keys(fields).forEach((f) => {
        // TODO: Replace with a propper RegExp implementation
        const searchedFor = `{{${f}}}`;

        while (raw.indexOf(searchedFor) > -1) {
          raw = raw.replace(searchedFor, fields[f]);
        }
      });
    }

    if (plural) {
      console.error(new Error(`Unable to handle plural parameters in translate() for id: ${id}`));
    }

    return raw;
  }

  @computed
  public get scope() {
    return this.internalScope;
  }
  public set scope(scope: string) {
    this.internalScope = scope;
  }

  @computed
  public get definition() {
    return this.definitions[this.innerLocale] || this.definitions[I18nStore.DefaultLocale];
  }

  @computed
  public get loading() {
    return this.isLoading;
  }

  @action
  public async change(newLocale: string, pathPrefix: string = '/locales/', suffix: string = '.json') {
    const l = newLocale.toLocaleLowerCase();

    // Stop early on requests to change the locale
    if (this.innerLocale === l) {
      return true;
    }

    // Mark the store as doing an async job
    this.isLoading = true;

    // Start fetching the locale data
    import(`${pathPrefix}${l}${suffix}`).
      then((definition) => {
        runInAction(() => {
          // Add the new definition to the list
          this.definitions[l] = definition;
          this.innerLocale = l;

          // Stop loading
          delayInAction(() => this.isLoading = false);
        });
      }).
      catch(() => {
        // Stop loading without changing the language
        delayInAction(() => this.isLoading = false);
      });

    return true;
  }

  private getRawText(definition: IDefinition, segments: string[], i: number): string {
    const v = definition[segments[i]];

    if (v) {
      if (typeof v !== 'string') {
        return this.getRawText(v as IDefinition, segments, i + 1);
      } else {
        return v;
      }
    }

    return '';
  }
}
