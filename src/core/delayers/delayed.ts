export type DelayType = 'timeout' | 'animation';
export type DelayedFn = () => void;

export class Delayed {
  private method: DelayType;
  private descriptor: number;
  private canceled: boolean = false;

  public constructor(method: DelayType, fn: DelayedFn, after: number = 0) {
    this.method = method;

    if (this.method === 'timeout') {
      this.descriptor = setTimeout(fn, after) as any;
    } else {
      this.descriptor = window.requestAnimationFrame(fn);
    }
  }

  public cancel() {
    if (!this.canceled) {
      if (this.method === 'timeout') {
        clearTimeout(this.descriptor);
      } else {
        window.cancelAnimationFrame(this.descriptor);
      }

      this.canceled = true;

      return true;
    }

    return false;
  }
}
