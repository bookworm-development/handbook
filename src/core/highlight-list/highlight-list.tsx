import { Component, h } from 'preact';

import { ElementChild } from '../ui/src';
import { Div } from '../ui/src/containers';
import { Grid, IGridProps } from '../ui/src/containers/grid';

import './highlight-list.scss';

interface IHighlightListProps {
  options?: IGridProps;
  items: IHighlightItem[];
}

export interface IHighlightItem {
  main: ElementChild;
  details: ElementChild;
}

export class HighlightList extends Component<IHighlightListProps, any> {
  public render() {
    return <Grid size='collapse' width='child-1-3' {...this.props.options || {}}>
      {
        this.props.items.map((i) => {
          return <Div flex={['row', 'middle']}>
              <Div text={['center', 'middle', 'emphasis']}
                style={{ fontSize: '4em', lineHeight: '0px' }}>
                {i.main}
              </Div>
              <Div text={['middle', 'uppercase', 'small']} padding={['small', 'remove-left']}
                class={'force-left-margin'}>
                {i.details}
              </Div>
            </Div>;
        })
      }
    </Grid>;
  }
}
