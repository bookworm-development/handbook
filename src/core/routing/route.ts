import { Component } from 'preact';

import { IconType } from '../ui/src';

interface IRouteBase {
  label: string;
  icon?: IconType;
  path: string; // The path portion this particular route is adding
  absolutePath?: string[];
  noAuthentication?: boolean; // All routes are checked for the user to be authenticated by default except this ones
  rights: IRouteRight[]; // All expected rights for a given route
}

const generatedRights: IRouteRight[][] = [
    'administrator', 'seller',
  ].map((userType: UserType) => {
    return ['create', 'read', 'update', 'delete', 'list', 'archive'].map((action: RouteAction) => {
      return {
        action,
        userType,
      } as IRouteRight;
    });
  });

export const AllRights: IRouteRight[] = ([] as IRouteRight[]).concat(...generatedRights);

export interface IStaticRoute extends IRouteBase {
  layout: new () => Component<any, any>; // Layout component of the route
}

export interface IDynamicRoute extends IRouteBase {
   layout: string | (new () => Component<any, any>); // Class name of the layout
   async: true;
   resourcePath: string; // Path to the resource relative to the application folder
}

export type Route = IStaticRoute | IDynamicRoute;

interface IRouteRight {
  userType: UserType;
  action: RouteAction;
}

type RouteAction = 'create' | 'read' | 'update' | 'delete' | 'list' | 'archive';
type UserType = 'administrator' | 'seller';

export interface IRoutes {
  notAuthorized: Route; // Default unauthorized route (used as fallback to authenticate a user)
  notFound: Route; // Default page not found (404) layout
  routes: IRouteHierarchyEntry[];
}

interface IRouteHierarchyEntry {
  route: Route;
  children?: IRouteHierarchyEntry[];
}

/**
 * Given a routes  hierarchy searches for the route matching the path segments
 * Supports successive searches in parent -> child relationships
 */
export function searchRoute(routes: IRouteHierarchyEntry[], path: string[], idx = 0): IRouteHierarchyEntry | null {
  const p = path[idx];

  for (const route of routes) {
    if (route.route.path === p) {
      if (route.children && (path.length - 1) > idx) {
        return searchRoute(route.children, path, idx + 1);
      }

      return route;
    }
  }

  return null;
}

/**
 * Given an IRoutes object, normalize the routes to include a meaningfull absolutePath
 */
export function normalizeRoutes(routes: IRoutes): IRoutes {
  return {
    notAuthorized: routes.notAuthorized,
    notFound: routes.notAuthorized,
    routes: cacheRoutePath(routes.routes),
  } as IRoutes;
}

function cacheRoutePath(routes: IRouteHierarchyEntry[], parentPaths: string[] = []): IRouteHierarchyEntry[] {
  return routes.map((r) => {
    const currentPath = [...parentPaths, r.route.path];

    r.route.absolutePath = currentPath;

    if (r.children && r.children.length) {
      r.children = cacheRoutePath(r.children, currentPath);
    }

    return r;
  });
}
