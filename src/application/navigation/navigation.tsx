import { Component, h } from 'preact';

// import { Text } from '../../../../core/intl';
import { Text } from '../../core/intl';
import { computed, inject, observer } from '../../core/store';
import { Headings, Icons, Logo, Navigation } from '../../core/ui/src';
import { INavItem } from '../../core/ui/src/navigation/nav/items/item';
import { AuthStore } from '../aaa/auth';
import '../aaa/auth';
import { routes } from '../application.routes';

interface IMainNavigationProps {
  auth?: AuthStore;
}

@inject('auth')
@observer
export class MainNavigation extends Component<IMainNavigationProps, any> {
  public render() {
    return <Navigation.NavBar transparent centered
      left={
        <Navigation.Nav navBar items={this.routes.map((r) => {
          return {
            ...r.icon ? { icon: r.icon } : {},
            label: r.label,
            location: r.path,
            type: 'item',
          } as INavItem;
        })} /> }
      center={
        <Logo>
          <Icons.Icon icon={Icons.storage['file-text']} margin={['small-bottom']} padding='remove' ratio={3} />
        </Logo>
      }
      right={<Navigation.Nav navBar items={[
        {
          icon: Icons.app['sign-out'],
          label: 'logout',
          location: '/',
          type: 'item',
        } as INavItem,
      ]} />} />;
  }

  @computed
  private get routes() {
    return routes.routes.filter((r) => (this.props.auth as AuthStore).isRouteAvailable(r.route)).map((r) => r.route);
  }
}
