import { createHashHistory } from 'history';
import { Component, h } from 'preact';
import * as AsyncRoute from 'preact-async-route';
import { route, Router, RouterOnChangeArgs } from 'preact-router';

import { Route } from '../core/routing';
import { IDynamicRoute } from '../core/routing/route';
import { action, computed, inject, observer } from '../core/store';
import { AuthStore } from './aaa/auth/auth.store';
import './aaa/auth/auth.store'; // Import for the side effects
import { routes } from './application.routes';

interface IApplicationRouterProps {
  auth?: AuthStore;
}

@inject('auth')
@observer
export class ApplicationRouter extends Component<IApplicationRouterProps, any> {
  private unauthorizedURLAttempt: string;

  public render() {
    return <Router onChange={this.route} history={createHashHistory()}>
      {
        this.routes.map((r) => {
          if ((r as IDynamicRoute).async) {
            // Async route
            const rr = r as IDynamicRoute;

            return <AsyncRoute path={rr.path} {
              ...typeof rr.layout === 'string' ?
                { getComponent: () => import(rr.resourcePath).then((module) => module[rr.layout as string]) } :
                { component: rr.layout }
              } />;
          } else {
            // Static route
            const C = r.layout as new() => Component<any, any>;

            return <C path={r.path} />;
          }
        })
      }
    </Router>;
  }

  @computed
  private get routes(): Route[] {
    const auth = this.props.auth as AuthStore;

    return [
      // Add the authentication page first
      routes.notAuthorized,

      // Add all top level routes available to the current user and context
      ...routes.routes.filter((r) => auth.isRouteAvailable(r.route)).map((r) => r.route),
    ];
  }

  @action.bound
  private async route(e: RouterOnChangeArgs) {
    const auth = this.props.auth as AuthStore;

    switch (e.url) {
      case '/main':

        break;
      case '/logout':
        return route('/', true);
      default:
        // TODO: Redirect to 404
        break;
    }

    // Check authentication before allowing routing
    if (!auth.loggedIn) {
      if (routes.notAuthorized.path !== e.url) {
        // Remember the intended path to navigate to before redirecting to login
        this.unauthorizedURLAttempt = e.url;
        console.info('User is not logged in, cought an attempt for: ' + this.unauthorizedURLAttempt);

        return route('/', true);
      }
    } else if (this.unauthorizedURLAttempt) {
      // Attempt to forward the user to the previously intended destination after a successful login
      const url = this.unauthorizedURLAttempt;
      this.unauthorizedURLAttempt = '';

      if (this.unauthorizedURLAttempt !== routes.notAuthorized.path) {
        return route(url, true);
      }
    }
  }
}
