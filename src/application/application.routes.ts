import { AllRights, IRoutes } from '../core/routing';
import { normalizeRoutes } from '../core/routing/route';
import { Icons } from '../core/ui/src/core';
import { LoginLayout } from './aaa/auth/login/login.layout';
import { RightsLayout } from './aaa/rights';
import { UsersLayout } from './aaa/users';
import { ClientsLayout } from './clients';
import { MainLayout } from './main/main.layout';
import { BarCodesLayout } from './products/barcodes';
import { StockActivityLayout } from './stocks';
import { TransactionsLayout } from './stocks/transaction';

export const routes: IRoutes = normalizeRoutes({
  notAuthorized: {
    // Use the login layout to allow authentications and authorization
    label: 'Login',
    layout: LoginLayout,
    noAuthentication: true,
    path: '',
    rights: AllRights,
  },
  notFound: {
    // TODO: Change to an actual 404 page later on
    label: 'Not Found',
    layout: LoginLayout,
    noAuthentication: true,
    path: '',
    rights: AllRights,
  },
  routes: [
    {
      route: {
        async: true,
        icon: Icons.app.home,
        label: 'Home',
        layout: MainLayout, // If code splitted, replace by the equivalent name
        path: 'main',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      route: {
        async: true,
        icon: Icons.app.users,
        label: 'Clients',
        layout: ClientsLayout,
        path: 'clients',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      route: {
        async: true,
        icon: Icons.app.future,
        label: 'Statistics',
        layout: MainLayout,
        path: 'statistics',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      route: {
        async: true,
        icon: Icons.app.social,
        label: 'Products',
        layout: MainLayout,
        path: 'products',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      children: [
        {
          route: {
            async: true,
            icon: Icons.app.history,
            label: 'Activity',
            layout: StockActivityLayout,
            path: 'activity',
            resourcePath: './main/main.layout',
            rights: AllRights,
          },
        },
        {
          route: {
            async: true,
            icon: Icons.storage.album,
            label: 'Stock',
            layout: MainLayout,
            path: 'stock',
            resourcePath: './main/main.layout',
            rights: AllRights,
          },
        },
        {
          route: {
            async: true,
            icon: Icons.storage.push,
            label: 'Transaction',
            layout: TransactionsLayout,
            path: 'transaction',
            resourcePath: './main/main.layout',
            rights: AllRights,
          },
        },
      ],
      route: {
        async: true,
        icon: Icons.storage.database,
        label: 'Stocks',
        layout: MainLayout,
        path: 'stocks',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      route: {
        async: true,
        icon: Icons.app.code,
        label: 'BarCode',
        layout: BarCodesLayout,
        path: 'barcodes',
        resourcePath: './main/main.layout',
        rights: AllRights,
      },
    },
    {
      children: [
        {
          route: {
            async: true,
            icon: Icons.app.user,
            label: 'Users',
            layout: UsersLayout,
            path: 'users',
            resourcePath: './aaa/users/users.layout',
            rights: AllRights,
          },
        },
        {
          route: {
            async: true,
            icon: Icons.app.lock,
            label: 'Rights',
            layout: RightsLayout,
            path: 'rights',
            resourcePath: './settings/settings.layout',
            rights: AllRights,
          },
        },
        {
          route: {
            async: true,
            icon: Icons.storage.database,
            label: 'Limits',
            layout: MainLayout,
            path: 'limits',
            resourcePath: './settings/settings.layout',
            rights: AllRights,
          },
        },
        {
          route: {
            async: true,
            icon: Icons.app.bell,
            label: 'Alerts',
            layout: MainLayout,
            path: 'alerts',
            resourcePath: './settings/settings.layout',
            rights: AllRights,
          },
        },
      ],
      route: {
        async: true,
        icon: Icons.app.cog,
        label: 'Settings',
        layout: MainLayout,
        path: 'settings',
        resourcePath: '',
        rights: AllRights,
      },
    },
    {
      route: {
        icon: Icons.app['sign-out'],
        label: 'Logout',
        layout: LoginLayout,
        noAuthentication: true,
        path: '/logout',
        rights: AllRights,
      },
    },
  ],
});
