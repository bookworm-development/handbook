import { h } from 'preact';

import { IUser } from '../aaa/users/user.type';
import { IBarCode } from './barcodes/barcode.type';

export interface IProduct {
  barCode: IBarCode;
  brand: string;
  category: IProductCategory;
  created: number;
  createdBy: IUser;
  description: string;
  id: string;
  updated: number;
  updatedBy: IUser;
  meta: IProductMeta;
}

interface IProductMeta {
  brand: string;
  description: string;
  color?: string;
  sizes?: ProductSize[];
}

type ProductSize = 'xxs' | 'xs' | 's' | 'm' | 'l' | 'xl' | 'xxl';

export interface IProductCategory {
  identifier: string;
  id: string;
}

export function asProductLabel(product: IProduct) {
  return (
    <span>
      { product.category.identifier } { product.brand ? product.brand : product.id } { product.description ? product.description : null }
    </span>
  );
}
