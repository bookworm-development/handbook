import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../../core/forms/form.custom';
import { I18nStore } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { computed, Containers, Headings } from '../../../core/ui/src';
import { deepClone } from '../../../core/util';
import { ISectionStorage } from '../../main/section/section.layout';
import { IBarCode } from './barcode.type';
import { BarcodesForm } from './barcodes.form';
import { UpdateBarCodesStore } from './barcodes.form.update.store';

interface IBarCodeDetailsProps {
  storage: ISectionStorage<IBarCode>;
  i18n: I18nStore;
  route: Route;
  traps?: ICustomFormActionTraps;
}

export class BarCodesDetails extends Component<IBarCodeDetailsProps, any> {
  public render() {
    const item = this.item;

    return <Containers.Div overflow='auto' width='1-1' animation={['fade']}>
      <Headings.H3 text={['top', 'emphasis']}
        padding='remove' margin='top' divider>
        { item.id }
      </Headings.H3>
      <BarcodesForm layout='horizontal' action='update'
        overwrite={new UpdateBarCodesStore(this.item)}
        actionTraps={this.props.traps || {}} />
    </Containers.Div>;
  }

  @computed
  private get item() {
    return deepClone(this.props.storage.item);
  }
}
