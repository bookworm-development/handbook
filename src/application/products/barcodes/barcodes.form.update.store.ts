import { action } from '../../../core/store';
import { BarCodes, IBarCode, KnownBarCodes } from './barcode.type';
import { BarCodesStore } from './barcodes.form.store';

export class UpdateBarCodesStore extends BarCodesStore {
  @action
  public resetFields() {
    super.resetFields();

    if (this.initialValues) {
      (Object.keys(this.initialValues) as Array<keyof IBarCode>).forEach((k) => {
        if (
            k === 'type' &&
            (!this.initialValues[k] || KnownBarCodes.indexOf(this.initialValues[k] as BarCodes) === -1)
          ) {
          // Fix barcodes with a missing type
          (this.values as any)[k].value = KnownBarCodes[0];
        } else {
          (this.values as any)[k].value = (this.initialValues as any)[k];
        }
      });
    }
  }
}
