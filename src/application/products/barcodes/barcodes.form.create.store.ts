import { format, getDayOfYear } from 'date-fns';

import { action, BaseStore, computed } from '../../../core/store';
import { AuthStore } from '../../aaa/auth';
import { IUser } from '../../aaa/users';
import { BarCodesStore } from './barcodes.form.store';

export class CreateBarcodeStore extends BarCodesStore {
  @action
  public resetFields() {
    super.resetFields();

    this.values.createdBy = {
      loading: false,
      value: ((BaseStore.getInstane().getStore('auth') as AuthStore).user || {} as IUser),
    };

    this.values.id = {
      loading: false,
      value: this.generateEAN13Code,
    };
  }

  @computed
  private get generateEAN13Code() {
    const today = new Date();
    const year = parseInt(format(today, 'YY'), 10);
    const day = getDayOfYear(today);
    const sequence = (
      year * 100000 + day * 10000 + today.getHours() * 1000 + today.getMinutes() * 10 + today.getSeconds()
      ).toString().split('').map((n) => parseInt(n, 10));

    const code = [
      // Country code
      4, 0,
      // Company (3 out of 5 used)
      1, 2, 3,
      // Sequence (7 out of 5 used)
      ...sequence,
    ];

    // Compute the check digit
    let sum = 0;
    for (let i = 0; i < 12; i += 1) {
      if (undefined === code[i]) {
        code.push(0);
      }

      sum += i % 2 === 1 ? code[i] * 3 : code[i];
    }

    code.push(((Math.floor(sum / 10) + 1) * 10 - sum) % 10);

    return code.join('');
  }
}
