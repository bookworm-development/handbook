import { CustomForm, Field, h, ICustomFormActions, ICustomFormProps, Inputs } from '../../../core/forms';
import { Text } from '../../../core/intl';
import { BarCode } from '../../../core/jsbarcode-preact/src';
import { computed, inject, observer } from '../../../core/store';
import { Containers } from '../../../core/ui/src';
import { Select } from '../../../core/ui/src/form/select/select';
import { IBarCode, KnownBarCodes } from './barcode.type';
import { BarCodesStore } from './barcodes.form.store';

import './barcodes.form.store';

interface IBarcodesFormProps extends ICustomFormProps<IBarCode> {
  store: BarCodesStore;
  action: 'create' | 'update' | 'view';
}

@inject((stores) => ({
  store: stores.rights,
}))
@observer
export class BarcodesForm extends CustomForm<IBarCode, Partial<IBarcodesFormProps>, any> {
  private static CreateBtnLabel = 'products.barcodes.form.submitBtnCreate';
  private static FieldCreated = 'products.barcodes.form.created';
  private static FieldCreatedBy = 'products.barcodes.form.createdBy';
  private static FieldType = 'products.barcodes.form.type';
  private static FieldCode = 'products.barcodes.form.code';

  @computed
  protected get actions() {
    if (this.props.action === 'create') {
      return {
        submitBtn: {
          handler: this.store.create,
          label: <Text id={BarcodesForm.CreateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    }

    return {
      resetBtn: null,
      submitBtn: null,
    } as ICustomFormActions;
  }

  @computed
  protected get fields() {
    const value = this.store.fieldMeta('id');
    const type = this.store.fieldMeta('type');

    return [
      // Code (id)
      <Field label={<Text id={BarcodesForm.FieldCode} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
          {...this.props.action !== 'create' ? { readonly: true } : {}}
          { ...value } />
      </Field>,

      // Created
      <Field label={<Text id={BarcodesForm.FieldCreated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('created') } />
      </Field>,

      // CreatedBy
      <Field label={<Text id={BarcodesForm.FieldCreatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('createdBy') } />
      </Field>,

      // Type
      <Field label={<Text id={BarcodesForm.FieldType} />}>
        <Select options={this.generateTypeOptions} {...type}
          {...this.props.action !== 'create' ? { disabled: true } : {}} />
        <Containers.Div flex={['middle', 'center']}>
          <BarCode type={type.value.get()} value={value.value.get()} containerType={'svg'}
            options={{
              background: '#222',
              lineColor: '#ffffffe3',
            }} />
        </Containers.Div>
      </Field>,
    ];
  }

  @computed
  private get generateTypeOptions() {
    return KnownBarCodes.map((t) => ({
      label: t,
      value: t,
    }));
  }
}
