import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../../core/forms/form.custom';
import { I18nStore, Text } from '../../../core/intl';
import { Containers, Headings } from '../../../core/ui/src';
import { BarcodesForm } from './barcodes.form';
import { CreateBarcodeStore } from './barcodes.form.create.store';

interface IBarcodesCreateProps {
  i18n: I18nStore;
  traps?: ICustomFormActionTraps;
}

export class BarCodesCreate extends Component<IBarcodesCreateProps, any> {
  private static CreateLabel = 'products.barcodes.create.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
        <Text id={BarCodesCreate.CreateLabel}
          fields={{ label: (this.props.i18n as I18nStore).translate('routes.barcodes.singular') }} />
      </Headings.H3>
      <BarcodesForm layout='horizontal' action='create' overwrite={new CreateBarcodeStore()}
        actionTraps={this.props.traps} />
    </Containers.Div>;
  }
}
