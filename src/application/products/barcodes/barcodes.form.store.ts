import { format } from 'date-fns';

import { I18nStore } from '../../../core/intl';
import { action, BaseStore, computed, observable, runInAction } from '../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../core/store/form';
import { Notifications } from '../../../core/ui/src';
import { asReferenceString, firebaseDB, resolveReference } from '../../../firebase';
import { IUser } from '../../aaa/users';
import { IBarCode } from './barcode.type';

export class BarCodesStore extends FormStore<IBarCode> {
  private static DateFormat = 'DD MMM YYYY HH:mm';

  protected validationRules: IFieldsValidationRules = {
    id: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 3 },
    },
  };

  protected transformersOut = {
    created: (val: any) => format(val, BarCodesStore.DateFormat) as any,
    createdBy: (val: any) => val.identifier,
  };

  @observable
  private progress: boolean;

  @action
  public resetFields() {
    this.values = {
      created: {
        loading: false,
        value: Date.now(),
      },
      createdBy: {
        loading: false,
        value: ({}) as IUser,
      },
      id: {
        loading: false,
        value: '',
      },
      type: {
        loading: false,
        value: 'EAN13',
      },
    };
  }

  @computed
  public get submitInProgress() {
    return this.progress;
  }

  @action.bound
  public async create(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    // Create a new barcode using the available data
    const rawBarCode = {
      created: this.values.created.value,
      createdBy: asReferenceString('users', this.values.createdBy.value.id),
      type: this.values.type.value,
    };

    this.progress = true;

    await firebaseDB.collection('barcodes').doc(this.values.id.value).set(rawBarCode);
    const barcodeRef = firebaseDB.doc(`barcodes/${this.values.id.value}`);
    const barcode = await resolveReference<IBarCode>(barcodeRef);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'barcodes',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'products.barcodes.notifications.created',
          { identifier: barcode.id },
        ),
        status: 'success' });
    });

    return barcode;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(BarCodesStore);
