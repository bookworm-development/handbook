import { IUser } from '../../aaa/users/user.type';

export interface IBarCode {
  type: BarCodes;
  id: string;
  created: number;
  createdBy: IUser;
}

export type BarCodes =
  'CODE128' | 'CODE128A' | 'CODE128B' | 'CODE128C' |
  'EAN13' | 'EAN8' | 'EAN5' | 'EAN2' | 'UPC' |
  'CODE39' |
  'ITF' | 'ITF14' |
  'MSI' | 'MSI10' | 'MSI11' | 'MSI1010' | 'MSI1110' |
  'pharmacode' |
  'codabar';

export const KnownBarCodes: BarCodes[] = [
  'CODE128', 'CODE128A', 'CODE128B', 'CODE128C',
  'EAN13', 'EAN8', 'EAN5', 'EAN2', 'UPC',
  'CODE39',
  'ITF', 'ITF14',
  'MSI', 'MSI10', 'MSI11', 'MSI1010', 'MSI1110',
  'pharmacode',
  'codabar',
];
