import { format, startOfDay } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { I18nStore, Text } from '../../../core/intl';
import { action, computed, inject, observer } from '../../../core/store';
import { Label } from '../../../core/ui/src';
import { listaAllOfCollection } from '../../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../../main/section/section.layout';
import { IProduct } from '../product.type';
import { IBarCode } from './barcode.type';
import { BarCodesCreate } from './barcodes.create';
import { BarCodesGlobalDetails } from './barcodes.globalDetails';
import { BarCodesDetails } from './barcodes.itemDetails';

interface IBarCodesLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

@inject('search', 'i18n', 'auth')
@observer
export class BarCodesLayout extends Section<IBarCode, IBarCodesLayoutProps, any> {
  private static ColumnsLabels = 'products.barcodes.grid.columns.';

  protected routePath = ['barcodes'];
  protected collection = 'barcodes';

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: this.createLayoutConfig,
      global: this.globalLayoutConfig,
      item: this.itemLayoutConfig,
    };
  }

  protected async fetchListData(): Promise<IBarCode[]> {
    return await listaAllOfCollection<IBarCode>('barcodes');
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<IBarCode>> {
    return [
      {
        field: 'id',
        label: <Text id={`${BarCodesLayout.ColumnsLabels}1`} />,
        render: this.renderField,
      },
      {
        field: 'created',
        label: <Text id={`${BarCodesLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'type',
        label: <Text id={`${BarCodesLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<IBarCode>> {
    return [
      {
        field: 'created',
        order: 'desc',
        valueFn: (created: number) => startOfDay(created).getTime(),
      },
      {
        field: 'type',
        order: 'asc',
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<IBarCode>>> {
    // Allow filtering by code bar value or type
    return [
      [
        {
          field: 'id',
          term: this.term,
        },
      ],
      [
        {
          field: 'type',
          term: this.term,
        },
      ],
    ];
  }

  @computed
  private get createLayoutConfig() {
    const layout = <BarCodesCreate i18n={this.props.i18n as I18nStore}
      traps={{
        submitBtn: {
          after: action((barCode: IBarCode) => {
            this.storage.list.push(barCode);
            this.globalDashboard();
          }),
        },
      }} />;

    return {
      layout,
    };
  }

  @computed
  private get globalLayoutConfig() {
    return {
      fetch: async () => await listaAllOfCollection<IProduct>('/products'),
      layout: (products: IProduct[]) =>
        <BarCodesGlobalDetails i18n={this.props.i18n as I18nStore}
          route={this.route} storage={this.storage} products={products} />,
    };
  }

  @computed
  private get itemLayoutConfig() {
    return {
      layout: <BarCodesDetails i18n={this.props.i18n as I18nStore} route={this.route} storage={this.storage} />,
    };
  }

  private renderField(value: any, key: keyof IBarCode) {
    switch (key) {
      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      case 'type':
        return <Label display='inline-block'>{value}</Label>;

      case 'id':
        return value;

      default:
        return null;
    }
  }
}
