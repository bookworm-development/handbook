import { Component, ComponentConstructor, h } from 'preact';

import { Inputs } from '../../core/forms';
import { Text } from '../../core/intl';
import { searchRoute } from '../../core/routing/route';
import { action, computed, inject, observable, observer, runInAction } from '../../core/store';
import { Containers, Icons, Link } from '../../core/ui/src';
import { AuthStore } from '../aaa/auth';
import { routes } from '../application.routes';
import { IMenuProps, Menu } from './menu/menu';
import { SearchStore } from './search.store';
import './search.store';

import * as background from '../../assets/main_background.jpg';
import './main.layout.scss';

interface IMainLayoutProps {
  auth?: AuthStore;
  search?: SearchStore;
}

@inject('auth', 'search')
@observer
export class MainLayout extends Component<Partial<IMainLayoutProps>, any> {
  private static SearchMessage = 'main.search';

  @observable
  private activePath: string;

  @observable
  private prevActivePath: string;

  @observable
  private contentOptions: IMenuProps['options'] = {};

  @observable
  private menuOptions: IMenuProps['options'] = {};

  @observable
  private submenuOptions: IMenuProps['options'] = {};

  private routesIdxMap: { [path: string]: number; };

  public render() {
    return <Containers.Div height='viewport' overflow='hidden'
      backgroundImage={background} background={['secondary', 'cover', 'center-center']}>
      <Containers.Container>
        <Containers.Container width='1-1' margin='large-top'>
          <Containers.Div text={'muted'}>
            <Text id={MainLayout.SearchMessage} />
          </Containers.Div>
          <Containers.Panel background={'default'} boxShadow='large' borderRadius='rounded'
            padding={['small', 'remove-top', 'remove-bottom']}>
            {
              // Back button
              this.active ?
                <Link
                  location='' styleType='text'
                  label={<Icons.Icon icon={Icons.direction['chevron-left']} ratio={3}
                  position={['center-left-out', 'z-index']} />}
                  handler={this.navigateBack} /> : null
            }

            <Inputs.Search
              width={['child-1-1', '1-1']}
              styleType='large'
              borderRadius='circle'
              boxShadow='bottom'
              searchIconFlip
              searchIconToggle
              searchIcon
              updateOn='onInput'
              forceAttributes={{ list: 'search_results' }}
              { ...(this.props.search as SearchStore).fieldMeta('term') } />
            <datalist id={'search_results'}>
              {
                (this.props.search as SearchStore).dataSet.map((d) => <option value={d} />)
              }
            </datalist>
          </Containers.Panel>

          {
            this.active ?
              this.submenuRoutes.length ?
                // Submenu
                <Menu
                  options={this.submenuOptions}
                  routes={this.submenuRoutes} childSize={'child-1-4'}
                  marginFn={() => true} logoPushFn={ (i) => i === 1 }
                  onItemClick={this.onItemClick} />
                : null
              :
              // Main menu
              <Menu
                activeFn={this.isActiveItem}
                options={this.menuOptions}
                routes={this.routes} childSize={'child-1-4'}
                marginFn={this.marginFn} logoPushFn={this.logoPushFn}
                onSubmenuClick={this.onCreateSubmenu} onItemClick={this.onItemClick} />
          }
        </Containers.Container>
      </Containers.Container>
      {
        // Embedded page
        this.active && this.submenuRoutes.length === 0 ?
          <Containers.Div margin={['small-top']} class='content-near-fill' boxShadow='large' overflow='hidden'
            {...this.contentOptions}>
            {this.content}
          </Containers.Div> : null
      }
    </Containers.Div>;
  }

  @computed
  protected get active() {
    return this.activePath;
  }
  protected set active(path: string) {
    this.prevActivePath = this.activePath;
    this.activePath = path;
  }

  private isActiveItem(i: number) {
    return i === 0;
  }

  @computed
  private get submenuRoutes() {
    if (
      this.active &&
      routes.routes[this.routesIdxMap[this.active]]
    ) {
      return (routes.routes[this.routesIdxMap[this.active]].children || []);
    }

    return [];
  }

  @computed
  private get routes() {
    this.routesIdxMap = {};

    return routes.routes.filter((r, i) => {
      const available = (this.props.auth as AuthStore).isRouteAvailable(r.route);

      if (available) {
        this.routesIdxMap[r.route.path] = i;
      }

      return available;
    });
  }

  private marginFn(i: number) {
    return (i < 4 && (i + 1) % 3 !== 0) || (i > 4 && (i + 2) % 3 !== 0 );
  }

  private logoPushFn(i: number) {
    return i === 4;
  }

  @action.bound
  private navigateBack(ev: MouseEvent) {
    ev.preventDefault();

    if (this.submenuRoutes.length) {
      // Submenu is active
      this.hideSubmenuAnd(
        () => {
          this.active = '';

          this.showMenu();
        },
      );
    } else {
      // Content is active
      this.hideContentAnd(
        () => {
          const prevPath = this.prevActivePath;

          if (!!prevPath) {
            // Previously in a submenu
            this.active = prevPath;

            this.showSubmenu();
          } else {
            // Go back to the main menu
            this.active = '';

            this.showMenu();
          }
        },
      );
    }
  }

  @action.bound
  private onCreateSubmenu(path: string) {
    this.hideMenuAnd(
      () => {
        this.active = path;

        this.showSubmenu();
      },
    );
  }

  @action.bound
  private onItemClick(path: string) {
    const showContent = () => {
      this.active = path;

      this.showContent();
    };

    if (this.submenuRoutes.length) {
      // Submenu is active
      this.hideSubmenuAnd(showContent);
    } else {
      // Menu is active
      this.hideMenuAnd(showContent);
    }
  }

  @action
  private hideMenuAnd(handle: HandleFn) {
    // Transition the current content out of view
    this.menuOptions = {};
    this.menuOptions.animation = ['slide-right', 'reverse'];

    this.menuOptions.onAnimationEnd = (_: AnimationEvent) => {
      // Got to the end of the animation, clean up
      runInAction(() => {
        handle();

        this.menuOptions = {};
      });
    };
  }

  @action
  private hideSubmenuAnd(handle: HandleFn) {
    // Transition the current content out of view
    this.submenuOptions = {};
    this.submenuOptions.animation = ['slide-right', 'reverse'];

    this.submenuOptions.onAnimationEnd = (_: AnimationEvent) => {
      // Got to the end of the animation, clean up
      runInAction(() => {
        handle();

        this.submenuOptions = {};
      });
    };
  }

  @action
  private hideContentAnd(handle: HandleFn) {
    // Transition the current content out of view
    this.contentOptions = {};
    this.contentOptions.animation = ['slide-right', 'reverse'];

    this.contentOptions.onAnimationEnd = (_: AnimationEvent) => {
      // Got to the end of the animation, clean up
      runInAction(() => {
        handle();

        this.contentOptions = {};
      });
    };
  }

  @action
  private showMenu() {
    this.menuOptions = {};
    this.menuOptions.animation = ['fade', 'fast'];
  }

  @action
  private showSubmenu() {
    this.submenuOptions = {};
    this.submenuOptions.animation = ['fade', 'fast'];
  }

  @action
  private showContent() {
    this.contentOptions = {};
    this.contentOptions.animation = ['fade', 'fast'];
  }

  @computed
  private get content() {
    let content: JSX.Element | JSX.Element[] | null = null;

    const r = this.prevActivePath === 'main' || !this.prevActivePath ?
      searchRoute(routes.routes, [this.active]) :
      searchRoute(routes.routes, [this.prevActivePath, this.active]);

    if (r) {
      // Search for the next section's component
      content = h(r.route.layout as ComponentConstructor, {});
    }

    return content;
  }
}

type HandleFn = () => void;
