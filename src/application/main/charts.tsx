export { Chart, Charts } from '../../core/chartjs-preact/src';

export const Colors = [
  '#4CAF50', '#D7263D', '#03A9F4', '#00E396',	'#F9CE1D', '#FF4560',	'#FF9800',
  '#A2C523', '#A43820', '#336B87', '#C6D166',
  '#486B00', '#BA5536', '#90AFC5', '#F62A00',
  '#2E4600', 'rgb(170, 170, 170)', '#2A3132', 'rgb(207, 207, 207)',
  '#7D4427',
];

export function someColors(ammount: number) {
  const n = Colors.length;

  return new Array(ammount).fill(0).map((_, i: number) => Colors[i % n ]);
}
