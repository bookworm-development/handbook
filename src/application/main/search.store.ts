import { action, BaseStore, computed, observable } from '../../core/store';
import { FormStore, IFieldsValidationRules } from '../../core/store/form';

export interface IFormStoreFields {
  term: string;
}

export interface ISearchContext {
  // Allow the calling context to set it's data based on it's own filtering
  data: (data: any[]) => void;

  // Allow the calling context to end the contextual search mode
  end: () => void;

  // Allow the calling context to set a reaction handler for search term changes
  onSearchTermChange: (fn: (term: string) => void) => void;
}

export class SearchStore extends FormStore<IFormStoreFields> {
  protected validationRules: IFieldsValidationRules = {
    term: {},
  };

  // TransformersIn allow transformation of raw input values (type casting most of the time)
  protected transformersIn = {
    term: {
      affected: [],
      transformation: (val: string) => {
        // Inform outside contexts of the
        if (this.contextualFn) {
          this.contextualFn(val);
        }

        return val;
      },
    },
  };

  @observable
  private data: any[] = [];

  @observable
  private contextual: boolean = false;

  private contextualFn: (term: string) => void;

  @action
  public resetFields() {
    this.values = {
      term: {
        loading: false,
        value: '',
      },
    };
  }

  @computed
  public get filter() {
    return this.values.term.value;
  }

  @action
  public startContextualSearch(): ISearchContext {
    this.contextual = true;

    return {
      data: action((data: any[]) => this.data = data),
      end: action(() => this.contextual = false),
      onSearchTermChange: action((fn: ((term: string) => void)) => this.contextualFn = fn),
    };
  }

  @action.bound
  public search() {
    if (this.contextual)  {
      // Do nothing for contextual searches
      return;
    }

    // Do the search!
  }

  @computed
  public get dataSet() {
    return this.data;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(SearchStore);
