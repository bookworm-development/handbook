import { Component, h } from 'preact';

import { Buttons, Icons } from '../../../core/forms';
import { Text } from '../../../core/intl';
import { computed, observer } from '../../../core/store';
import { ISectionButton } from './section.create.button';

@observer
export class GlobalButton extends Component<ISectionButton, any> {
  private static GlobalDashboardMessage = 'main.global';

  public render() {
    if (this.props.context === 'global') {
      return null;
    }

    return <Buttons.Button label={<Text id={GlobalButton.GlobalDashboardMessage} />} float='right' inverse='dark'
      handler={this.props.handler} size='small' styleType='secondary'
      icon={Icons.app.world}
      {...this.animation} />;
  }

  @computed
  private get animation(): { [p: string]: any; } {
    if (this.props.animated) {
      return {
        animation: 'fade',
      };
    }

    return {};
  }
}
