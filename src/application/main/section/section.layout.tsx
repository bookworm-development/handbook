import { Component, h } from 'preact';

import { delayInAction } from '../../../core/delayers';
import { DataGrid, ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { Text, I18nStore } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { searchRoute } from '../../../core/routing/route';
import { action, computed, observable, runInAction, BaseStore } from '../../../core/store';
import { Containers, ElementChild, Headings, Icons } from '../../../core/ui/src';
import { firebaseDB } from '../../../firebase';
import { AuthStore } from '../../aaa/auth';
import { routes } from '../../application.routes';
import { ISearchContext, SearchStore } from '../search.store';
import { CreateButton } from './section.create.button';
import { DeleteButton } from './section.delete.button';
import { GlobalButton } from './section.global.button';

export interface ISectionProps {
  auth?: AuthStore;
  search?: SearchStore;
}

interface ISectionLoadings {
  list: boolean; // List loading status only enforced on listings
  details: boolean; // Details loading status, enforced on create, update and get
}

export interface ISectionStorage<T> {
  list: T[]; // Lists
  item: T; // Individual details (forms)
  details: any; // Global details only
}

export type DetailsContext = 'global' | 'item' | 'create';

type LayoutGetter = ((attributes: { [p: string]: any }) => ElementChild);

interface IContextLayout {
  fetch?: () => Promise<any>;
  layout: ElementChild | LayoutGetter;
}

export type ContextualLayouts = {
  [layoutType in DetailsContext]: IContextLayout;
};

export abstract class Section<T, P extends ISectionProps, S> extends Component<P, S> {
  /**
   * No operation data fetch
   */
  private static async noOpFetch() {
    return Promise.resolve();
  }

  @observable
  protected term: string = '';

  @observable
  protected loading: ISectionLoadings = {
    details: false,
    list: true,
  };

  @observable
  protected storage: ISectionStorage<T> = {
    details: {} as any,
    item: {} as any,
    list: [],
  };

  @observable
  protected animated: { create: boolean; delete: boolean; global: boolean; } = {
    create: true, delete: false, global: false,
  };

  /**
   * Path of the current resource's route
   * Used to determine the current route
   */
  protected abstract routePath: string[];

  /**
   * Current resource's collection
   * Used to allow additions
   */
  protected abstract collection: string;

  private layoutAttributes: any;

  private searchContext: ISearchContext;

  @observable
  private innerContext: DetailsContext;

  public componentWillMount() {
    const search = this.props.search as SearchStore;
    this.term = search.filter;

    // Establish the contextual search
    this.searchContext = search.startContextualSearch();

    this.searchContext.onSearchTermChange(this.onSearchTermChange);
  }

  public componentDidMount() {
    // Get the intial data sets
    this.fetchDataList();

    // Set the initial details section to display the global overview (dashboard)
    this.detailContext = 'global';
  }

  public componentWillUnmount() {
    // Exit the contextual search
    this.searchContext.end();
  }

  public render() {
    const auth = this.props.auth as AuthStore;
    const route = this.route;

    return <Containers.Div style={{ height: '100%' }} overflow='hidden'>
      <Containers.Container style={{ height: '5.9%' }}>
        <Containers.Container>
          <Headings.H3 padding='small' flex='middle' display='block' text='capitalize'>
            <Icons.Icon icon={this.route.icon || Icons.app.question} ratio={1.5} margin='small-right' />
            <Text id={`routes.${route.path}.plural`} />

            <CreateButton
              animated={this.animated.create} auth={auth}
              context={this.detailContext} handler={this.createItem} route={route} />

            <DeleteButton
              animated={this.animated.delete} auth={auth} item={this.storage.item}
              context={this.detailContext} handler={this.deleteCurrentItem} route={route} />

            <GlobalButton
              animated={this.animated.global} auth={auth}
              context={this.detailContext} handler={this.globalDashboard} route={route} />
          </Headings.H3>
        </Containers.Container>
      </Containers.Container>
      <Containers.Grid  style={{ minHeight: '94.1%', maxHeight: '94.1%' }} overflow='auto'
        size='collapse' background='default' padding='remove' margin='remove'>

        <Containers.Div width='3-5' height={{ 'expand': true, 'offset-top': true, 'offset-bottom': true }}
          loading={this.loading.list}>

          <DataGrid categories={this.dataCategories} data={this.storage.list} sort={this.sortingRules}
            tableOptions={{ striped: true, align: 'middle' }}
            filter={this.term ? this.filteringRules : []}
            {...this.canSelect ? { selectable: true, onSelection: this.selectionChanged } : {}}
            empty={this.empty} />
        </Containers.Div>

        <Containers.Div width='2-5' padding={['small', 'remove-top']} inverse='light' background='secondary'
          height={{ 'expand': true, 'offset-top': true, 'offset-bottom': true }}
          loading={this.loading.list || this.loading.details}>
          {
            this.detailContextLayout
          }
        </Containers.Div>
      </Containers.Grid>
    </Containers.Div>;
  }

  @computed
  protected get route(): Route {
    const r = searchRoute(routes.routes, this.routePath);

    return (r as any).route;
  }

  @computed
  protected get detailContext() {
    return this.innerContext;
  }
  protected set detailContext(context: DetailsContext) {
    if (context !== this.innerContext)  {
      const layoutConfiguration = this.layouts[context];

      // Fetch any prerequisites for the current context
      const fetchData = layoutConfiguration.fetch || Section.noOpFetch;

      // Delay a data fetch for all required resources of the specified context and change the context afterwards
      delayInAction(async () => {
        // Enforce the loading status
        this.loading.details = true;

        const attributes = await fetchData();
        this.layoutAttributes = attributes;

        runInAction(() => {
          // Remove loading status
          this.loading.details = false;

          // Enforce the new context
          this.innerContext = context;
        });
      }, 0);
    }
  }

  /**
   * Contextual layouts configuration getter
   */
  protected abstract get layouts(): ContextualLayouts;

  protected abstract get dataCategories(): Array<ICategorySetting<T>>;

  protected abstract get sortingRules(): Array<ICategorySortRules<T>>;

  protected abstract get filteringRules(): Array<Array<ICategoryFilterRules<T>>>;

  protected abstract async fetchListData(): Promise<T[]>;

  @action.bound
  protected globalDashboard() {
    if (this.detailContext === 'item') {
      // Do not reanimate the create button, it's already visible
      this.animated.create = false;
    } else {
      // Animate the create button when exiting the create context
      this.animated.create = true;
    }

    this.selectionChanged(null);
  }

  @computed
  private get canSelect() {
    return (this.props.auth as AuthStore).roleAllowsTo(this.route, 'create');
  }

  @computed
  private get detailContextLayout() {
    let layout: IContextLayout['layout'];

    switch (this.detailContext) {
      case 'create':
        layout = this.layouts.create.layout;
        break;

      case 'global':
        layout = this.layouts.global.layout;
        break;

      case 'item':
        layout = this.layouts.item.layout;
        break;

      default:
        layout = null;
        break;
    }

    // In case of layout getters grab the layout by providing the attributes
    if (layout && (layout as LayoutGetter).call) {
      layout = (layout as LayoutGetter)(this.layoutAttributes);
    }

    return layout;
  }

  @action.bound
  private createItem() {
    // Animate the global button when entering create (allows it to be animated when exiting it)
    this.animated.global = true;

    this.detailContext = 'create';
  }

  @action.bound
  private selectionChanged(item: T | null) {
    if (item) {
      if (this.detailContext === 'item') {
        // Do not animate any of the buttons when changing between items
        this.animated.create = this.animated.delete = this.animated.global = false;
      } else {
        // Animate the delete and global buttons as they will apear into view when viewing an item
        this.animated.delete = this.animated.global = true;
      }

      this.storage.item = item;
      this.detailContext = 'item';
    } else {
      this.detailContext = 'global';
    }
  }

  @action
  private async fetchDataList() {
    const items = await this.fetchListData();

    this.storage.list = items;

    runInAction(() => {
      this.storage.list = items;
      this.loading.list = false;
    });
  }

  @action.bound
  private onSearchTermChange(term: string) {
    this.term = term;
  }

  @computed
  private get empty() {
    return <Containers.Div>
      <Icons.Icon icon={this.route.icon || Icons.app.question} ratio={1.5} margin='small-right' />
      <Text id={`main.section.empty`} fields={{ label:
        (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(`routes.${this.route.path}.plural`) }} />
    </Containers.Div>;
  }

  @action.bound
  private async deleteCurrentItem() {
    const id = (this.storage.item as any).id;

    await firebaseDB.doc(`${this.collection}/${id}`).delete();

    runInAction(() => {
      // Remove the item from the current list of items
      this.storage.list = this.storage.list.filter((i) => (i as any).id  !== id);

      // Change to the global context after finalizing the deletion
      this.globalDashboard();
    });
  }
}
