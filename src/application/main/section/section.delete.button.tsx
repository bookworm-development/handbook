import { Component, h } from 'preact';
import * as UiKit from 'uikit';

import { Buttons, Icons } from '../../../core/forms';
import { I18nStore, Text } from '../../../core/intl';
import { action, BaseStore, computed, inject, observer, runInAction } from '../../../core/store';
import { Containers, Modal } from '../../../core/ui/src';
import { ISectionButton } from './section.create.button';

export interface IDeleteButton<T> extends ISectionButton {
  handler: () => Promise<void>;
  item: T;
  i18n?: I18nStore;
}

@inject('i18n')
@observer
export class DeleteButton<T> extends Component<IDeleteButton<T>, any> {
  private static DeleteLabel = 'main.delete.label';
  private static DeleteTitle = 'main.delete.title';
  private static DeleteMessage = 'main.delete.message';
  private static DeleteMessageDetails = 'main.delete.messageDetails';
  private static DeleteConfirmBtn = 'main.delete.confirm';
  private static DeleteCancelBtn = 'main.delete.cancel';

  private modalId = BaseStore.GenerateUniqueIdentifier();

  public render() {
    const route = this.props.route;

    if (!this.canDelete || this.props.context !== 'item') {
      return null;
    }

    return <span>
      <Buttons.Button
        label={
          <span>
            <Icons.Icon icon={Icons.app.trash} ratio={1} />
            <Text id={DeleteButton.DeleteLabel} fields={{
            type: (this.props.i18n as I18nStore).translate(
              `routes.${route.path}.singular`,
            ),
          }} />
          </span>
        } float='right' inverse='dark'
        handler={this.noop} size='small' styleType='danger'
        toggle={{ target: `#${this.modalId}` }}
        {...this.animation} />

        <Modal close={false}
          id={this.modalId} center
          header={<Text id={DeleteButton.DeleteTitle} fields={{identifier: (this.props.item as any).identifier}} />}>
            <Text id={DeleteButton.DeleteMessage} /><br />
            <Text id={DeleteButton.DeleteMessageDetails} />
            <Containers.Grid width='child-1-2' padding='small' margin='remove'>
              <Modal.CloseButton label={<Text id={DeleteButton.DeleteCancelBtn} />} />
              <Buttons.Button label={<Text id={DeleteButton.DeleteConfirmBtn} />} styleType='danger'
                handler={this.delete} />
            </Containers.Grid>
          </Modal>
      </span>;
  }

  @action.bound
  private async delete() {
    await this.props.handler();

    runInAction(() => {
      const modal = UiKit.modal(`#${this.modalId}`);

      (modal as any).getActive().hide();
    });
  }

  @computed
  private get canDelete() {
    return this.props.auth.roleAllowsTo(this.props.route, 'delete');
  }

  @computed
  private get animation(): { [p: string]: any; } {
    if (this.props.animated) {
      return {
        animation: 'fade',
      };
    }

    return {};
  }

  private noop() {
    // Intentional no operation handler
  }
}
