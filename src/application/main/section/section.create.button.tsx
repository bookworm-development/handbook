import { Component, h } from 'preact';

import { Buttons, Icons } from '../../../core/forms';
import { I18nStore, Text } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { computed, inject, observer } from '../../../core/store';
import { AuthStore } from '../../aaa/auth';
import { DetailsContext } from './section.layout';

export interface ISectionButton {
  auth: AuthStore;
  context: DetailsContext;
  route: Route;
  handler: () => void;
  animated: boolean;
  i18n?: I18nStore;
}

@inject('i18n')
@observer
export class CreateButton extends Component<ISectionButton, any> {
  private static CreateMessage = 'main.create';

  public render() {
    const route = this.props.route;

    if (!this.canCreate || this.props.context === 'create') {
      return null;
    }

    return <Buttons.Button
      label={
        <span>
          <Icons.Icon icon={Icons.app.plus} ratio={.5} />
          <Icons.Icon icon={route.icon as any} margin='small-right' />
          <Text id={CreateButton.CreateMessage} fields={{
            type: (this.props.i18n as I18nStore).translate(
              `routes.${route.path}.singular`,
            ),
          }} />
        </span>
      } float='right' inverse='dark'
      handler={this.props.handler} size='small' styleType='primary'
      {...this.animation} />;
  }

  @computed
  private get canCreate() {
    return this.props.auth.roleAllowsTo(this.props.route, 'create');
  }

  @computed
  private get animation(): { [p: string]: any; } {
    if (this.props.animated) {
      return {
        animation: 'fade',
      };
    }

    return {};
  }
}
