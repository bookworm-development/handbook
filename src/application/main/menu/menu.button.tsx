import { Component, h } from 'preact';

import { Text } from '../../../core/intl';
import { computed, observer } from '../../../core/store';
import { ElementChildOnly, Icons, IconType } from '../../../core/ui/src';
import { Containers, Link } from '../../../core/ui/src';

interface IMenuBtnProps {
  active?: boolean;
  margin?: boolean;
  colorClass: string;
  path: string;
  icon: IconType;
  label: ElementChildOnly;
  onClick?: (ev: MouseEvent) => void;
}

@observer
export class MenuBtn extends Component<IMenuBtnProps, any> {
  public render() {
    return <Containers.Card styleType='default' boxShadow={['medium', 'hover-large']}
      {...this.attributes}
      flex={['center', 'middle']}
      class={`grey ${this.props.colorClass}${this.props.active ? ' active' : ''}`}>
        {
          this.props.path === 'main' ?
            [
              <Icons.Icon icon={this.props.icon} ratio={4} flex='center' />,
              <Containers.Div flex='center' text='capitalize' userSelect='none'>
                <Text id={`routes.${this.props.path}.plural`} />
              </Containers.Div>,
            ] :
            <Link location={this.props.path} styleType='reset'
              handler={this.props.onClick}
              label={[
                <Icons.Icon icon={this.props.icon} ratio={4} flex='center' />,
                <Containers.Div flex='center' text='capitalize' userSelect='none'>
                  <Text id={`routes.${this.props.path}.plural`} />
                </Containers.Div>,
              ]} />
        }
    </Containers.Card>;
  }

  @computed
  protected get attributes() {
    return {
      // Margin
      ...this.props.margin ? { margin: 'small-right' } : {},
    };
  }
}
