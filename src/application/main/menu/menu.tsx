import { Component, h } from 'preact';

import { IRoutes } from '../../../core/routing';
import { action, observer } from '../../../core/store';
import { Icons, Images, Width } from '../../../core/ui/src';
import { Containers } from '../../../core/ui/src';
import { IGridProps } from '../../../core/ui/src/containers/grid';
import { MenuBtn } from './menu.button';

import * as NoteBookLogo from '../../../assets/logo.svg';

export interface IMenuProps {
  childSize: Width;
  routes: IRoutes['routes'];
  activeFn?: (i: number) => boolean;
  marginFn: (i: number) => boolean;
  logoPushFn: (i: number) => boolean;
  onItemClick?: (path: string) => void;
  onSubmenuClick?: (path: string) => void;
  options?: IGridProps;
}

@observer
export class Menu extends Component<IMenuProps, any> {
  public render() {
    const activeFn = this.props.activeFn ? this.props.activeFn : (_: number) => false;

    return <Containers.Container flex={['center', 'middle']}>
      <Containers.Grid size='small' width={['5-6@m', '1-1@l', this.props.childSize]} margin='large-top'
        heightMatch={{ row: false }} flex={['center', 'middle']}
        {...this.props.options || {}}>
        {
          this.props.routes.map((r, i) => {
            // Leaf level menu items first
            const card = <MenuBtn
              active={activeFn(i)}
              margin={this.props.marginFn(i)}
              icon={r.route.icon || Icons.app.question} label={r.route.label} path={r.route.path}
              colorClass={`grey--g${ i + 1 }`}
              onClick={
                r.children && r.children.length && this.props.onSubmenuClick ?
                  action((ev: MouseEvent) => {
                    ev.preventDefault();

                    (this.props.onSubmenuClick as (path: string) => void)(r.route.path);
                  }) :
                  this.props.onItemClick ?
                    action((ev: MouseEvent) => {
                      ev.preventDefault();

                      (this.props.onItemClick as (path: string) => void)(r.route.path);
                    }) : undefined
              } />;

            // Push the logo if required
            if (this.props.logoPushFn(i)) {
              return [
                <Containers.Div flex={['center', 'middle']} padding='remove' margin='small-right'
                  background={'default'} userSelect='none'>
                  <Images.Image src={NoteBookLogo} style={{ marginLeft: '-5%', width: '80%' }} />
                </Containers.Div>,
                card,
              ];
            }

            return card;
          })
        }
      </Containers.Grid>
    </Containers.Container>;
  }
}
