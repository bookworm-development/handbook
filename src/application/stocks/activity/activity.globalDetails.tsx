import {
  addHours,
  addMinutes,
  endOfDay,
  endOfMonth,
  endOfWeek,
  isWithinRange,
  startOfDay,
  startOfMonth,
  startOfWeek,
  subDays,
  subMonths,
  subWeeks,
  } from 'date-fns';
import { Component, h } from 'preact';

import { HighlightList } from '../../../core/highlight-list';
import { I18nStore, Text } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { action, computed, observable, observer } from '../../../core/store';
import { Buttons, Containers, Headings } from '../../../core/ui/src';
import { Charts, someColors } from '../../main/charts';
import { ISectionStorage } from '../../main/section/section.layout';
import { IStockActivity, KnownActivities, Operation } from './activity.type';

interface IStockActivityGlobalDetailsProps {
  storage: ISectionStorage<IStockActivity>;
  i18n: I18nStore;
  route: Route;
  // products: IProduct[];
}

type ChartMode = 'today' | 'yesterday' | 'thisWeek' | 'lastWeek' | 'thisMonth' | 'lastMonth';
const chartModes: ChartMode[] = ['today', 'yesterday', 'thisWeek', 'lastWeek', 'thisMonth', 'lastMonth'];

@observer
export class ActivityGlobalDetails extends Component<IStockActivityGlobalDetailsProps, any> {
  private static DetailsLabel = 'stocks.activity.details.label';
  private static StockOperations = 'stocks.activity.operations.';
  private static StockActivityChartTitle = 'stocks.activity.details.chart.title';
  private static ChartButtons = 'stocks.activity.details.chart.buttons.';

  @observable
  private chartMode: ChartMode = 'today';

  public render() {
    const coverages = this.coverages;

    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
          <Text id={ActivityGlobalDetails.DetailsLabel}
            fields={{ label: this.props.i18n.translate('routes.activity.plural') }} />
        </Headings.H3>
      <HighlightList options={{ width: 'child-1-4' }} items={
          KnownActivities.map((a) => ({
            details: <Containers.Div userSelect='none'
              text={ a === 'added' || a === 'sold' ? 'success' : a === 'blocked' ? 'danger' : 'warning' }>
                <Text id={`${ActivityGlobalDetails.StockOperations}${a}.plural`}></Text>
              </Containers.Div>,
            main: <Containers.Div text='muted' userSelect='none'>{coverages[a]}</Containers.Div>,
          }))
        }/>

      <hr />

      <Containers.Div padding={['large', 'remove-top', 'remove-bottom']}>
        <Charts.Line redraw data={this.stockActivitiesTimelineChartData as any} width={400} height={400} options={{
            animation: { animateRotate: true, animateScale: true },
            legend: { display: true, position: 'bottom' },
            responsive: true,
            scales: {
              xAxes: [{
                bounds: 'data',
                display: true,
                distribution: 'series',
                ticks: {
                  source: 'auto',
                },
                type: 'time',
              }],
              yAxes: [{
                display: true,
                label: 'Quantity',
              }],
            },
            title: {
              display: true,
              position: 'top',
              text: this.props.i18n.translate(
                ActivityGlobalDetails.StockActivityChartTitle,
                { period: this.props.i18n.translate(`${ActivityGlobalDetails.ChartButtons}${this.chartMode}`) },
              ),
            },
          } as any} />

        {
          ...chartModes.map((m) => {
            return <Buttons.Button label={<Text id={`${ActivityGlobalDetails.ChartButtons}${m}`} />}
              handler={ () => this.changeChartMode(m) } margin='small-top' width='1-2' size='small' />;
          })
        }
      </Containers.Div>
    </Containers.Div>;
  }

  @action.bound
  private changeChartMode(mode: ChartMode) {
    this.chartMode = mode;
  }

  @computed
  private get coverages() {
    const coverage = KnownActivities.reduce((ret, f) => {
      ret[f] = 0;

      return ret;
    }, {} as { [operation in Operation]: number; });

    this.props.storage.list.forEach((activity) => {
      coverage[activity.operation] += 1;
    });

    return coverage;
  }

  @computed
  private get chartRanges(): { [chartMode in ChartMode]: { start: Date, end: Date } } {
    const now = new Date();
    let start: Date;
    let end: Date;

    return chartModes.reduce((ret, mode) => {
      switch (mode) {
        case 'today':
          start = end = now;
          break;

        case 'yesterday':
          start = end = subDays(now, 1);
          break;

        case 'thisWeek':
          start = startOfWeek(now);
          end = endOfWeek(now);
          break;

        case 'lastWeek':
          const lastWeek = subWeeks(now, 1);
          start = startOfWeek(lastWeek);
          end = endOfWeek(lastWeek);
          break;

        case 'thisMonth':
          start = startOfMonth(now);
          end = endOfMonth(now);
          break;

        case 'lastMonth':
          const lastMonth = subMonths(now, 1);
          start = startOfMonth(lastMonth);
          end = endOfMonth(lastMonth);
      }

      ret[mode] = {
        end: endOfDay(end),
        start: startOfDay(start),
      };

      return ret;
    }, {} as { [chartMode in ChartMode]: { start: Date, end: Date } });
  }

  @computed
  private get stockToCurrentStartRange(): number {
    let stock = 0;

    const data = [
      ...this.props.storage.list,
      ...this.testOnlyData, // TODO: remove once debugging is done
    ].sort((a, b) => a.created - b.created);

    const start = this.chartRanges[this.chartMode].start.getTime();

    for (const a of data) {
      if (a.created < start) {
        switch (a.operation) {
          case 'added':
          case 'returned':
            stock += a.quantity;
            break;

          case 'sold':
          case 'blocked':
            stock -= a.quantity;
            break;
        }
      } else {
        // Break the loop when finding the first element of the currently selected period
        break;
      }
    }

    return stock;
  }

  @computed
  private get chartData(): IStockActivity[] {
    const range = this.chartRanges[this.chartMode];

    return [
      ...this.props.storage.list,
      ...this.testOnlyData, // TODO: remove once debugging is done
    ].
    sort((a, b) => a.created - b.created).
    filter((a) => {
      return isWithinRange(a.created, range.start, range.end);
    });
  }

  @computed
  private get stockActivitiesTimelineChartData() {
    let stock = this.stockToCurrentStartRange;
    const variations: {[o in Operation]: Array<{x: Date, y: number}>} = KnownActivities.reduce((obj, o) => {
      obj[o] = [];

      return obj;
    }, {} as {[o in Operation]: Array<{x: Date, y: number}>});

    const data = this.chartData;
    let lastDataPoint: {x: Date, y: number};

    data.forEach((a) => {
      const date = new Date(a.created);

      switch (a.operation) {
        case 'added':
        case 'returned':
          stock += a.quantity;
          break;

        case 'sold':
        case 'blocked':
          stock -= a.quantity;
          break;
      }

      KnownActivities.forEach((operation) => {
        const isCurrentOperation = operation === a.operation;
        const point = {
          x: date,
          y: isCurrentOperation ? stock : NaN,
        };

        if (isCurrentOperation) {
          if (lastDataPoint) {
            const lastOfPrevOperation = variations[operation][variations[operation].length - 1];

            if (isNaN(lastOfPrevOperation.y)) {
              lastOfPrevOperation.y = lastDataPoint.y;
            }
          }

          lastDataPoint = point;
        }

        variations[operation].push(point);
      });
    });

    const colors: string[] = someColors(KnownActivities.length);

    return {
      datasets: [
        ...KnownActivities.map((o, i) => ({
          backgroundColor: colors[i],
          borderColor: colors[i],
          borderWidth: 1,
          data: variations[o],
          fill: false,
          label: this.props.i18n.translate(`stocks.activity.operations.${o}.singular`),
          steppedLine: 'before',
        })),
      ],
      // labels,
    };
  }

  @computed
  private get testOnlyData() {
    let date: Date = this.chartRanges.lastMonth.start;
    const end = endOfDay(new Date());
    const data: IStockActivity[] = [];
    let i = 0;

    while (date < end) {
      data.push({
        created: date.getTime(),
        createdBy: this.props.storage.list[0].createdBy,
        id: this.props.storage.list[0].id,
        operation: KnownActivities[i % 4],
        price: Math.floor(Math.random() * 100),
        product: this.props.storage.list[0].product,
        quantity: Math.floor(Math.random() * 30),
      });

      i++;
      const minute = i % 60;
      const minutes = Math.floor(Math.random() * 1000000) % 5 !== 0 && minute !== 0;

      date = minutes ? addMinutes(date, minute) : addHours(date, i % 24);
    }

    return data;
  }
}
