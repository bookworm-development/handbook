import { format } from 'date-fns';

import { I18nStore } from '../../../core/intl';
import { action, BaseStore, computed, observable, runInAction } from '../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../core/store/form';
import { Notifications } from '../../../core/ui/src';
import { asReferenceString, firebaseDB, resolveReference, listaAllOfCollection } from '../../../firebase';
import { IUser } from '../../aaa/users';
import { IProduct } from '../../products/product.type';
import { IStockActivity } from './activity.type';

export class StockActivityStore extends FormStore<IStockActivity> {
  private static DateFormat = 'DD MMM YYYY HH:mm';
  private productsMap: { [productId: string]: IProduct };

  protected validationRules: IFieldsValidationRules = {
    price: {
      max: { max: Infinity },
      min: { min: 0 },
    },
    quantity: {
      max: { max: Infinity },
      min: { min: 0 },
    },
  };

  protected transformersIn = {
    price: {
      affected: ['price'],
      transformation: (val: string) => parseInt(val, 10) || 0,
    },
    product: {
      affected: ['product', 'price'],
      transformation: async (id: string) => {
        if (!this.productsMap) {
          // Grab the products map initially
          const products = await listaAllOfCollection<IProduct>('products');

          this.productsMap = products.reduce((val, p) => {
            val[p.id] = p;

            return val;
          }, {} as {} as  { [productId: string]: IProduct });
        }

        return this.productsMap[id];
      },
    },
    quantity: {
      affected: ['quantity'],
      transformation: (val: string) => parseInt(val, 10) || 0,
    },
  };

  protected transformersOut = {
    created: (val: any) => format(val, StockActivityStore.DateFormat) as any,
    createdBy: (val: any) => val.identifier,
    product: (val: any) => val ? val.id : '',
  };

  @observable
  private progress: boolean;

  @action
  public resetFields() {
    this.values = {
      created: {
        loading: false,
        value: Date.now(),
      },
      createdBy: {
        loading: false,
        value: {} as IUser,
      },
      id: {
        loading: false,
        value: '',
      },
      operation: {
        loading: false,
        value: 'sold',
      },
      price: {
        loading: false,
        value: 0,
      },
      product: {
        loading: false,
        value: {} as IProduct,
      },
      quantity: {
        loading: false,
        value: 0,
      },
    };

    this.mergeInitialValues();
  }

  @computed
  public get submitInProgress() {
    return this.progress;
  }

  @action.bound
  public async create(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    // Create a new activity using the available data
    const rawActivity = {
      created: this.values.created.value,
      createdBy: asReferenceString('users', this.values.createdBy.value.id),
      operation: this.values.operation.value,
      price: this.values.price.value,
      product: asReferenceString('products', this.values.product.value.id),
      quantity: this.values.quantity.value,
    };

    this.progress = true;

    const activityRef = await firebaseDB.collection('stockActivity').add(rawActivity);
    const activity = await resolveReference<IStockActivity>(activityRef);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'activity',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'stocks.activity.notifications.created',
          { identifier: `${this.values.product.value.id}::${activity.price}` },
        ),
        status: 'success' });
    });

    return activity;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(StockActivityStore);
