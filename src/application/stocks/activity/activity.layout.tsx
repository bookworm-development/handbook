import { format, startOfDay } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { I18nStore, Text } from '../../../core/intl';
import { computed, inject, observer } from '../../../core/store';
import { Label } from '../../../core/ui/src';
import { listaAllOfCollection } from '../../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../../main/section/section.layout';
import { ActivityGlobalDetails } from './activity.globalDetails';
import { IStockActivity } from './activity.type';

interface IStockActivityLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

@inject('search', 'i18n', 'auth')
@observer
export class StockActivityLayout extends Section<IStockActivity, IStockActivityLayoutProps, any> {
  private static ColumnsLabels = 'stocks.activity.grid.columns.';
  private static StockOperations = 'stocks.activity.operations.';

  protected routePath = ['stocks', 'activity'];
  protected collection = 'stockActivity';

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: { layout: null },
      global: this.globalLayoutConfig,
      item: { layout: null },
    };
  }

  protected async fetchListData(): Promise<IStockActivity[]> {
    return await listaAllOfCollection<IStockActivity>('stockActivity');
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<IStockActivity>> {
    return [
      {
        field: 'created',
        label: <Text id={`${StockActivityLayout.ColumnsLabels}1`} />,
        render: this.renderField,
      },
      {
        field: 'product',
        label: <Text id={`${StockActivityLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'operation',
        label: <Text id={`${StockActivityLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
      {
        field: 'price',
        label: <Text id={`${StockActivityLayout.ColumnsLabels}4`} />,
        render: this.renderField,
      },
      {
        field: 'quantity',
        label: <Text id={`${StockActivityLayout.ColumnsLabels}5`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<IStockActivity>> {
    return [
      {
        field: 'created',
        order: 'desc',
        valueFn: (created: number) => startOfDay(created).getTime(),
      },
      {
        field: 'product',
        order: 'asc',
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<IStockActivity>>> {
    // Allow filtering
    return [
      [
        {
          field: 'product',
          term: this.term,
        },
      ],
      [
        {
          field: 'price',
          term: this.term,
        },
      ],
    ];
  }

  @computed
  private get globalLayoutConfig() {
    return {
      // fetch: async () => await listaAllOfCollection<IProduct>('/products'),
      layout: <ActivityGlobalDetails i18n={this.props.i18n as I18nStore}
          route={this.route} storage={this.storage} />,
    };
  }

  private renderField(value: any, key: keyof IStockActivity) {
    switch (key) {
      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      case 'product':
        return value.id;

      case 'operation':
        return <Label styleType={value === 'added' || value === 'sold' ? 'success' : 'warning'}>
          <Text id={`${StockActivityLayout.StockOperations}${value}.singular`}></Text>
        </Label>;

      case 'price':
        return value;

      case 'quantity':
        return value;

      default:
        return null;
    }
  }
}
