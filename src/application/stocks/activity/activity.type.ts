import { IUser } from '../../aaa/users/user.type';
import { IProduct } from '../../products/product.type';

export interface IStockActivity {
  created: number;
  createdBy: IUser;
  operation: Operation;
  price: number;
  product: IProduct;
  quantity: number;
  id: string;
}

export type Operation =
  'added' | // Added to stock
  'blocked' | // Blocked stock (in customers custody)
  'returned' | // Returned stock (from the customer's custody)
  'sold'; // Removed from the stock due to a sale or stock removal (price change for example)

export const KnownActivities: Operation[] = [
  'added', 'blocked', 'returned', 'sold',
];
