import { CustomForm, Field, h, ICustomFormActions, ICustomFormProps, Inputs } from '../../../core/forms';
import { I18nStore, Text } from '../../../core/intl';
import { computed, inject, observer } from '../../../core/store';
import { Tables } from '../../../core/ui/src';
import { Select } from '../../../core/ui/src/form/select/select';
import { asProductLabel, IProduct } from '../../products/product.type';
import { StockActivityStore } from './activity.form.store';
import { IStockActivity, KnownActivities } from './activity.type';

import './activity.form.store';

interface IActivityFormProps extends ICustomFormProps<IStockActivity> {
  i18n?: I18nStore;
  store: StockActivityStore;
  action: 'create' | 'view';
  products: IProduct[];
  prices: { [productId: string]: number[] };
}

@inject((stores) => ({
  i18n: stores.i18n,
  store: stores.rights,
}))
@observer
export class StockActivityForm extends CustomForm<IStockActivity, Partial<IActivityFormProps>, any> {
  private static FieldProduct = 'stocks.activity.form.product';
  private static FieldOperation = 'stocks.activity.form.operation';
  private static FieldPrice = 'stocks.activity.form.price';
  private static FieldQuantity = 'stocks.activity.form.quantity';

  @computed
  protected get actions() {
    return {
      resetBtn: null,
      submitBtn: null,
    } as ICustomFormActions;
  }

  @computed
  protected get fields() {
    if (this.props.action === 'view') {
      return this.viewOnlyFields;
    }

    return [
      // Product
      <Field label={<Text id={StockActivityForm.FieldProduct} />} width='1-1'>
        <Select options={this.productOptions as any[]}
          {...this.props.action !== 'create' ? { readonly: true } : {}}
          {...this.store.fieldMeta('product')} />
      </Field>,

      // Operation
      <Field label={<Text id={StockActivityForm.FieldOperation} />} width='2-3'>
        <Select options={this.operationOptions}
          {...this.props.action !== 'create' ? { readonly: true } : {}}
          {...this.store.fieldMeta('operation')} />
      </Field>,

      <Field width={'1-3'} />,

      // Price
      <Field label={<Text id={StockActivityForm.FieldPrice} />} width='1-2'>
        <Inputs.Input list='prices'
          {...this.props.action !== 'create' ? { readonly: true } : {}}
          {...this.store.fieldMeta('price')} />

        <datalist id='prices'>{ ...this.priceOptions }</datalist>
      </Field>,

      // Quantity
      <Field label={<Text id={StockActivityForm.FieldQuantity} />} width='1-2'>
        <Inputs.Input autocomplete='off' blank
          {...this.props.action !== 'create' ? { readonly: true } : {}}
          {...this.store.fieldMeta('quantity')} />
      </Field>,
    ];
  }

  @computed
  protected get viewOnlyFields() {
    return <Tables.Row>
      <Tables.Cell>
        { this.store.fieldMeta('created').value }
      </Tables.Cell>
      <Tables.Cell>
      { this.store.fieldMeta('createdBy').value }
      </Tables.Cell>
      <Tables.Cell>
        { this.store.fieldMeta('operation').value }
      </Tables.Cell>
      <Tables.Cell>
        { this.store.fieldMeta('product').value }
      </Tables.Cell>
      <Tables.Cell>
        { this.store.fieldMeta('price').value }
      </Tables.Cell>
      <Tables.Cell>
        { this.store.fieldMeta('quantity').value }
      </Tables.Cell>
    </Tables.Row>;
  }

  @computed
  private get operationOptions() {
    return KnownActivities.map((t) => (
      {
        label: (this.props.i18n as I18nStore).translate(`stocks.activity.operations.${t}.singular`),
        value: t,
      }
    ));
  }

  @computed
  private get productOptions() {
    return (this.props.products || []).map((p) => (
      {
        label: asProductLabel(p),
        value: p.id,
      }
    ));
  }

  private get prices() {
    const product = this.store.fieldMeta('product').value.get();

    return (this.props.prices || {})[product] || [];
  }

  @computed
  private get priceOptions() {


    return this.prices.map((p) => (
      <option value={p} />
    ));
  }
}
