import { action, BaseStore } from '../../../core/store';
import { AuthStore } from '../../aaa/auth';
import { IUser } from '../../aaa/users';
import { TransactionsStore } from './transactions.form.store';

export class UpdateTransactionsStore extends TransactionsStore {
  @action
  public resetFields() {
    super.resetFields();

    this.values.updatedBy = {
      loading: false,
      value: ((BaseStore.getInstane().getStore('auth') as AuthStore).user || {} as IUser),
    };

    this.values.updated = {
      loading: false,
      value: Date.now(),
    };
  }
}
