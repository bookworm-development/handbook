import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../../core/forms/form.custom';
import { I18nStore, Text } from '../../../core/intl';
import { Containers, Headings } from '../../../core/ui/src';
import { TransactionsForm } from './transactions.form';
import { UpdateTransactionsStore } from './transactions.form.update.store';

interface ITransactionsCreateProps {
  i18n: I18nStore;
  traps?: ICustomFormActionTraps;
}

export class TransactionsUpdate extends Component<ITransactionsCreateProps, any> {
  private static UpdateLabel = 'stocks.transaction.create.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
        <Text id={TransactionsUpdate.UpdateLabel}
          fields={{ label: (this.props.i18n as I18nStore).translate('routes.transaction.singular') }} />
      </Headings.H3>
      <TransactionsForm layout='horizontal' action='create' overwrite={new UpdateTransactionsStore()}
        actionTraps={this.props.traps} />
    </Containers.Div>;
  }
}
