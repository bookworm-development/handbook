import { action, BaseStore } from '../../../core/store';
import { AuthStore } from '../../aaa/auth';
import { IUser } from '../../aaa/users';
import { TransactionsStore } from './transactions.form.store';

export class CreateTransactionsStore extends TransactionsStore {
  @action
  public resetFields() {
    super.resetFields();

    this.values.createdBy = {
      loading: false,
      value: ((BaseStore.getInstane().getStore('auth') as AuthStore).user || {} as IUser),
    };

    this.values.created = {
      loading: false,
      value: Date.now(),
    };
  }
}
