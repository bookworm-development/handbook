import { format, startOfDay } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { I18nStore, Text } from '../../../core/intl';
import { action, computed, inject, observable, observer } from '../../../core/store';
import { Label } from '../../../core/ui/src';
import { DocumentReference, listaAllOfCollection, resolveReference } from '../../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../../main/section/section.layout';
import { IProduct, IProductCategory } from '../../products/product.type';
import { IStockActivity, KnownActivities, Operation } from '../activity/activity.type';
import { IStock } from '../stock/stock.type';
import { ITransaction } from './transaction.type';
import { TransactionsCreate } from './transactions.create';

interface ITransactionsLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

@inject('search', 'i18n', 'auth')
@observer
export class TransactionsLayout extends Section<ITransaction, ITransactionsLayoutProps, any> {
  private static ColumnsLabels = 'stocks.transaction.grid.columns.';
  private static Status = 'stocks.transaction.status.';
  private static Operation = 'stocks.transaction.operations.';

  private static determineActivities(value: IStockActivity[]) {
    const ammounts: { [operation in Operation]: number } = KnownActivities.reduce((ret, a) => {
      ret[a] = 0;

      return ret;
    }, {} as { [operation in Operation]: number });

    value.forEach((a) => {
      ammounts[a.operation] += 1;
    });

    return KnownActivities.filter((a) => ammounts[a]).map((activityName) => {
      return <Label>
        <Text id={`${TransactionsLayout.Operation}${activityName}.singular`} />
      </Label>;
    });
  }

  protected routePath = ['stocks', 'transaction'];
  protected collection = 'transactions';

  @observable
  protected products: IProduct[] = [];

  @observable
  protected prices: { [productId: string]: number[] } = {};

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: this.createLayoutConfig,
      global: this.globalLayoutConfig,
      item: { layout: null },
    };
  }

  protected async fetchListData(): Promise<ITransaction[]> {
    const transactions = await listaAllOfCollection<ITransaction>('transactions');

    // Resolve the activities references
    const activities: IStockActivity[][] = await Promise.all(transactions.map((t) => {
      return Promise.all(t.activities.map((a) => {
        return resolveReference<IStockActivity>(a as any as DocumentReference);
      }));
    }));

    activities.forEach((a, i) => {
      transactions[i].activities = a;
    });

    // Grab all products at list level, as they will be useful for all actions in the current section
    this.products = await this.fetchProducts();

    const stocks = await this.fetchStocks();
    this.prices = stocks.reduce((val, s) => {
      const [product, price] = s.id.split('_');
      const p = parseInt(price, 10);

      val[product] = val[product] || [];

      if (val[product].indexOf(p) === -1) {
        val[product].push(p);
      }

      return val;
    }, {} as { [productId: string]: number[] });

    return transactions;
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<ITransaction>> {
    return [
      {
        field: 'created',
        label: <Text id={`${TransactionsLayout.ColumnsLabels}1`} />,
        render: this.renderField,
      },
      {
        field: 'status',
        label: <Text id={`${TransactionsLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'ammount',
        label: <Text id={`${TransactionsLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
      {
        field: 'activities',
        label: <Text id={`${TransactionsLayout.ColumnsLabels}4`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<ITransaction>> {
    return [
      {
        field: 'created',
        order: 'desc',
        valueFn: (created: number) => startOfDay(created).getTime(),
      },
      {
        field: 'status',
        order: 'asc',
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<ITransaction>>> {
    // Allow filtering
    return [
      [
        {
          field: 'status',
          term: this.term,
        },
      ],
      [
        {
          field: 'ammount',
          term: this.term,
        },
      ],
    ];
  }

  @computed
  private get globalLayoutConfig() {
    return {
      layout: null,
    };

    // return {
    //   // fetch: async () => await listaAllOfCollection<IProduct>('/products'),
    //   layout: <ActivityGlobalDetails i18n={this.props.i18n as I18nStore}
    //       route={this.route} storage={this.storage} />,
    // };
  }

  @computed
  private get createLayoutConfig() {
    return {
      layout: <TransactionsCreate i18n={this.props.i18n as I18nStore}
      products={this.products}
      prices={this.prices}
      traps={{
        submitBtn: {
          after: action((transaction: ITransaction) => {
            this.storage.list.push(transaction);
            this.globalDashboard();
          }),
        },
      }} />,
    };
  }

  private renderField(value: any, key: keyof ITransaction) {
    switch (key) {
      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      case 'status':
        const styleType =
          value === 'fianlized' ? 'success' :
          value === 'pending' ? 'warning' :
          'danger';

        return <Label styleType={styleType}>
          <Text id={`${TransactionsLayout.Status}${value}`}></Text>
        </Label>;

      case 'ammount':
        return value;

      case 'activities':
        return TransactionsLayout.determineActivities(value);

      default:
        return null;
    }
  }

  private async fetchProducts(): Promise<IProduct[]> {
    const products = await listaAllOfCollection<IProduct>('products');
    const categories = await listaAllOfCollection<IProductCategory>('productCategories');
    const categoriesMap: { [categoryId: string]: IProductCategory } = categories.reduce((obj, c) => {
      obj[c.id] = c;

      return obj;
    }, {} as { [categoryId: string]: IProductCategory });

    return products.map((p) => {
      p.category = categoriesMap[p.category.id];

      return p;
    });
  }

  private async fetchStocks(): Promise<IStock[]> {
    return await listaAllOfCollection<IStock>('stock');
  }
}
