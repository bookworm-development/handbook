import { IUser } from '../../aaa/users/user.type';
import { IStockActivity } from '../activity/activity.type';

export interface ITransaction {
  /**
   * Activities array
   * Will aways contain the same type of activities
   */
  activities: IStockActivity[];

  created: number;

  createdBy: IUser;

  updated: number | null;

  updatedBy: IUser | null;

  /**
   * Transaction status
   * Can be one of:
   * - fianlized: when a successful sale or added operations set has completed successfully
   * - pending: when the operations are in blocked state
   * - canceled: when the operation is a return (might mean cash returns also or partial return of goods)
   */
  status: TransactionStatus;

  /**
   * Ammount prepayed in case of pending transactions
   */
  ammount: number;

  id: string;
}

export type TransactionStatus = 'fianlized' | 'pending' | 'canceled';

export const KnownTransactionStatuses: TransactionStatus[] = [
  'canceled', 'fianlized', 'pending',
];
