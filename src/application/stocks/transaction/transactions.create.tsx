import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../../core/forms/form.custom';
import { I18nStore, Text } from '../../../core/intl';
import { Containers, Headings, observer } from '../../../core/ui/src';
import { IProduct } from '../../products/product.type';
import { TransactionsForm } from './transactions.form';
import { CreateTransactionsStore } from './transactions.form.create.store';

interface ITransactionsCreateProps {
  i18n: I18nStore;
  traps?: ICustomFormActionTraps;
  products: IProduct[];
  prices: { [productId: string]: number[] };
}

@observer
export class TransactionsCreate extends Component<ITransactionsCreateProps, any> {
  private static CreateLabel = 'stocks.transaction.create.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
        <Text id={TransactionsCreate.CreateLabel}
          fields={{ label: (this.props.i18n as I18nStore).translate('routes.transaction.singular') }} />
      </Headings.H3>
      <TransactionsForm layout='horizontal' action='create' overwrite={new CreateTransactionsStore()}
        actionTraps={this.props.traps} products={this.props.products} prices={this.props.prices} />
    </Containers.Div>;
  }
}
