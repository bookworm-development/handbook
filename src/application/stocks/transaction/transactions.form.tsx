import { CustomForm, Field, h, ICustomFormActions, ICustomFormProps, Inputs } from '../../../core/forms';
import { I18nStore, Text } from '../../../core/intl';
import { action, computed, inject, observer } from '../../../core/store';
import { Buttons, Containers, Headings, Tables } from '../../../core/ui/src';
import { Select } from '../../../core/ui/src/form/select/select';
import { IProduct } from '../../products/product.type';
import { StockActivityForm } from '../activity/activity.form';
import { StockActivityStore } from '../activity/activity.form.store';
import { IStockActivity } from '../activity/activity.type';
import { ITransaction, KnownTransactionStatuses } from './transaction.type';
import { TransactionsStore } from './transactions.form.store';

import './transactions.form.store';

interface ITransactionsFormProps extends ICustomFormProps<ITransaction> {
  store: TransactionsStore;
  action: 'create' | 'update' | 'view';
  i18n?: I18nStore;
  products: IProduct[];
  prices: { [productId: string]: number[] };
}

@inject((stores) => ({
  i18n: stores.i18n,
  store: stores.rights,
}))
@observer
export class TransactionsForm extends CustomForm<ITransaction, Partial<ITransactionsFormProps>, any> {
  private static CreateBtnLabel = 'stocks.transaction.form.submitBtnCreate';
  private static UpdateBtnLabel = 'stocks.transaction.form.submitBtnUpdate';
  private static FieldCreated = 'stocks.transaction.form.created';
  private static FieldCreatedBy = 'stocks.transaction.form.createdBy';
  private static FieldUpdated = 'stocks.transaction.form.updated';
  private static FieldUpdatedBy = 'stocks.transaction.form.updatedBy';
  private static FieldAmmountPaid = 'stocks.transaction.form.ammount';
  private static FieldStatus = 'stocks.transaction.form.status';
  private static FieldNewActivity = 'stocks.transaction.form.newActivity';

  @computed
  protected get actions() {
    if (this.props.action === 'create') {
      return {
        submitBtn: {
          handler: this.store.create,
          label: <Text id={TransactionsForm.CreateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    } else if (this.props.action === 'update') {
      return {
        submitBtn: {
          handler: this.store.update,
          label: <Text id={TransactionsForm.UpdateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    }

    return {
      resetBtn: null,
      submitBtn: null,
    } as ICustomFormActions;
  }

  @computed
  protected get fields() {
    const updated = this.store.fieldMeta('updated').value.get();
    const activities: IStockActivity[] = this.store.fieldMeta('activities').value.get();
    const initialActivities: IStockActivity[] = (this.store as TransactionsStore).initialActivities;
    const n = initialActivities.length;

    return [
      // Created
      <Field label={<Text id={TransactionsForm.FieldCreated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('created') } />
      </Field>,

      // CreatedBy
      <Field label={<Text id={TransactionsForm.FieldCreatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('createdBy') } />
      </Field>,

      ...updated ? [
        // Updated
        <Field label={<Text id={TransactionsForm.FieldUpdated} />}>
          <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
            { ...this.store.fieldMeta('updated') } />
        </Field>,

        // UpdateddBy
        <Field label={<Text id={TransactionsForm.FieldUpdatedBy} />}>
          <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
            { ...this.store.fieldMeta('updatedBy') } />
        </Field>,
      ] : [],

      // Ammount
      <Field label={<Text id={TransactionsForm.FieldAmmountPaid} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('ammount') } />
      </Field>,

      // Status
      <Field label={<Text id={TransactionsForm.FieldStatus} />}>
        <Select options={this.statusOptions} {...this.store.fieldMeta('status')} disabled />
      </Field>,

      // Activities
      <Headings.H3>Activities</Headings.H3>,

      // Existing activities
      <Tables.Table>
        {
          ...initialActivities.map((a) => {
            return <StockActivityForm overwrite={new StockActivityStore(a)} action='view' />;
          })
        }
      </Tables.Table>,

      // Newly added activities
      ...activities.slice(n).map((a, i) => {
        return <Containers.Div style={{ backgroundColor: '#354648' }} inverse='light'
          padding={['small', 'remove-bottom']} margin='small-bottom'>
          <StockActivityForm overwrite={
            new StockActivityStore(a, { parent: this.store, path: ['activities', `${i + n}`] })}
            action='create' grid gridSize='small'
            prices={this.props.prices}
            products={this.props.products} />
        </Containers.Div>;
      }),

      // Add a new activity to the list
      ...!updated ? [
        <Buttons.Button
          label={<Text id={TransactionsForm.FieldNewActivity} />}
          handler={this.addActivity} />,
      ] : [],
    ];
  }

  @computed
  private get statusOptions() {
    return KnownTransactionStatuses.map((t) => {
      return {
        label: (this.props.i18n as I18nStore).translate(`stocks.transaction.form.statusValues.${t}`),
        value: t,
      };
    });
  }

  @action.bound
  private addActivity() {
    this.store.addActivity((this.props.products || [])[0] || null, this.props.prices);
  }
}
