import { format } from 'date-fns';

import { I18nStore } from '../../../core/intl';
import { action, BaseStore, computed, observable, runInAction } from '../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../core/store/form';
import { Notifications } from '../../../core/ui/src';
import {
  asReferenceString, asReferenceStringFrom, DocumentReference,
  firebaseDB, getReferenceTo, resolveReference } from '../../../firebase';
import { AuthStore } from '../../aaa/auth';
import { IUser } from '../../aaa/users';
import { IProduct } from '../../products/product.type';
import { IStockActivity } from '../activity/activity.type';
import { IStock } from '../stock/stock.type';
import { ITransaction } from './transaction.type';

export class TransactionsStore extends FormStore<ITransaction> {
  private static DateFormat = 'DD MMM YYYY HH:mm';

  protected validationRules: IFieldsValidationRules = {
    activities: {
      count: { min: 1 },
    },
  };

  protected transformersOut = {
    created: (val: any) => val && format(val, TransactionsStore.DateFormat) as any || '',
    createdBy: (val: any) => val && val.identifier || '',
    updated: (val: any) => val && format(val, TransactionsStore.DateFormat) as any || '',
    updatedBy: (val: any) => val && val.identifier || '',
  };

  @observable
  private progress: boolean;

  @action
  public resetFields() {
    this.values = {
      activities: {
        loading: false,
        value: [],
      },
      ammount: {
        loading: false,
        value: 0,
      },
      created: {
        loading: false,
        value: Date.now(),
      },
      createdBy: {
        loading: false,
        value: {} as IUser,
      },
      id: {
        loading: false,
        value: '',
      },
      status: {
        loading: false,
        value: 'fianlized',
      },
      updated: {
        loading: false,
        value: null,
      },
      updatedBy: {
        loading: false,
        value: null,
      },
    };

    this.mergeInitialValues();
  }

  @computed
  public get submitInProgress() {
    return this.progress;
  }

  @computed
  public get initialActivities(): IStockActivity[] {
    return this.initialValues && this.initialValues.activities as IStockActivity[] || [];
  }

  @action.bound
  public addActivity(product: IProduct | null, prices: { [productId: string]: number[] }) {
    const p = product || { id: '' };
    const pPrices = prices[p.id] || [];
    const price = pPrices.slice().sort()[pPrices.length - 1] || 0;

    this.activities.get().push({
      created: Date.now(),
      createdBy: (BaseStore.getInstane().getStore('auth') as AuthStore).user as IUser,
      operation: 'added',
      price,
      product,
      quantity: 0,
    });
  }

  @action.bound
  public async create(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    // Add all stock activities, change the stocks and create the stock transaction
    const user = (BaseStore.getInstane().getStore('auth') as AuthStore).user as IUser;
    const userStringRef = asReferenceString('users', user.id);

    const rawTransaction = {
      activities: [] as DocumentReference[],
      ammount: this.values.ammount.value,
      created: this.values.created.value,
      createdBy: userStringRef,
      status: this.values.status.value,
    };

    this.progress = true;

    const activities = this.values.activities.value as IStockActivity[];

    // Grab a reference to all stock entries affected
    const stocks: IStock[] = await Promise.all(activities.map((a) => {
      return resolveReference<IStock>(getReferenceTo('stock', `${a.product.id}_${a.price}`));
    }));

    // Create a map of stocks
    const stocksMap: { [stockId: string]: IStock } = {};
    stocks.forEach((s) => stocksMap[s.id] = s);

    // Create all activities and update stocks
    await Promise.all(activities.map(async (a) => {
      // Create the stock activity
      const activity = await firebaseDB.collection('stockActivity').add({
        created: this.values.created.value,
        createdBy: userStringRef,
        operation: a.operation,
        price: a.price,
        product: asReferenceString('products', a.product.id),
        quantity: a.quantity,
      });

      // Add activities to the transaction
      rawTransaction.activities.push(activity);

      // Update or create the stock (should always exist if the product was created using the UI)
      const stockId = `${a.product.id}_${a.price}`;
      const pStock = stocksMap[stockId] || {};

      const stock = {
        created: this.values.created.value,
        createdBy: userStringRef,
        lastActivity: asReferenceStringFrom(activity),
        product: asReferenceString('products', a.product.id),
        quantity: (
          pStock.quantity ?
            typeof pStock.quantity === 'string' && parseInt(pStock.quantity, 10) :
            pStock.quantity
          ) || 0,
        rebased: !!pStock.rebased,
      };

      if (a.operation === 'added' || a.operation === 'returned') {
        // TODO: Add corner case when selling a pending item, it should not reduce the stock twice
        stock.quantity += a.quantity;
      } else {
        stock.quantity -= a.quantity;
      }

      return firebaseDB.doc(asReferenceString('stock', stockId)).set(stock);
    }));

    // Create the stock transaction
    const transactionRef = await firebaseDB.collection('transactions').add(rawTransaction);
    const transaction = await resolveReference<ITransaction>(transactionRef);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'activity',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'stocks.transactions.notifications.created',
          { identifier: `${transaction.id}` },
        ),
        status: 'success' });
    });

    return transaction;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(TransactionsStore);
