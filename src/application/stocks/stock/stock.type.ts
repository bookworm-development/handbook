import { IProduct } from '../../products/product.type';
import { IStockActivity } from '../activity/activity.type';
import { IUser } from '../../aaa/users';

export interface IStock {
  created: number;
  createdBy: IUser;
  product: IProduct; // Product
  quantity: number; // Quantity in stock
  rebased: boolean; // Was the price rebased?
  id: string; // Stock entry id of format productID_entryPrice
  lastActivity: IStockActivity; // Last activity applied on the stock - debug only
}
