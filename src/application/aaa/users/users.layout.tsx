import { format } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { I18nStore, Text } from '../../../core/intl';
import { computed, inject, observer } from '../../../core/store';
import { listaAllOfCollection, searchOrHitCache } from '../../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../../main/section/section.layout';
import { IRole } from '../rights';
import { UserGlobalDetails } from './user.globalDetails';
import { UserDetails } from './user.itemDetails';
import { IUser } from './user.type';

interface IUserLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

@inject('search', 'i18n', 'auth')
@observer
export class UsersLayout extends Section<IUser, IUserLayoutProps, any> {
  private static ColumnsLabels = 'aaa.users.grid.columns.';

  protected routePath = ['settings', 'users'];
  protected collection = 'users';

  @computed
  protected get globalDetails() {
    return <UserGlobalDetails i18n={this.props.i18n as I18nStore} route={this.route} storage={this.storage} />;
  }

  @computed
  protected get itemDetails() {
    return <UserDetails i18n={this.props.i18n as I18nStore} route={this.route} storage={this.storage} />;
  }

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: { layout: null },
      global: { layout: null },
      item: { layout: null },
    };
  }

  protected async fetchListData(): Promise<IUser[]> {
    const users = await listaAllOfCollection<IUser>('users');
    const roles = await listaAllOfCollection<IRole>('roles');
    const rolesMap = {};

    users.forEach((u) => {
      u.role = searchOrHitCache(roles, u.role.id, rolesMap);

      if (!u.role) {
        console.info('User missing role', u);
        u.role = roles[0];
      }
    });

    return users;
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<IUser>> {
    return [
      {
        field: 'identifier',
        label: <Text id={`${UsersLayout.ColumnsLabels}1`} />,
        options: {
          expand: true,
        },
        render: this.renderField,
      },
      {
        field: 'role',
        label: <Text id={`${UsersLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'created',
        label: <Text id={`${UsersLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<IUser>> {
    return [
      {
        field: 'identifier',
        order: 'asc',
      },
      {
        field: 'role',
        order: 'desc',
      },
      {
        field: 'created',
        order: null,
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<IUser>>> {
    return [
      [
        {
          field: 'identifier',
          term: this.term,
        },
      ],
      [
        {
          field: 'role',
          term: this.term,
        },
      ],
      [
        {
          field: 'created',
          term: this.term,
        },
      ],
    ];
  }

  private renderField(value: any, key: keyof IUser) {
    switch (key) {
      case 'identifier':
        return value.toString();

      case 'role':
        return value.identifier;

      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      default:
        return null;
    }
  }
}
