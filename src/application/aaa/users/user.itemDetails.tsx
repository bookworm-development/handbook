import { format } from 'date-fns';
import { Component, h } from 'preact';

import { I18nStore, Text } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { Containers, ElementChild, Headings, Label, Lists, Tables } from '../../../core/ui/src';
import { searchOrHitCache } from '../../../firebase';
import { ISectionStorage } from '../../main/section/section.layout';
import { IUser } from './user.type';

interface IUserGlobalDetailsProps {
  storage: ISectionStorage<IUser>;
  i18n: I18nStore;
  route: Route;
}

export class UserDetails extends Component<IUserGlobalDetailsProps, any> {
  private static SettingsLanguageLabel = 'aaa.users.itemDetails.settings.lang';
  private static FieldIdentifier = 'aaa.users.itemDetails.fields.identifier';
  private static FieldCreated = 'aaa.users.itemDetails.fields.created';
  private static FieldCreatedBy = 'aaa.users.itemDetails.fields.createdBy';
  private static FieldLastAuthenticated = 'aaa.users.itemDetails.fields.lastAuthenticated';
  private static FieldRole = 'aaa.users.itemDetails.fields.role';
  private static FieldSettings = 'aaa.users.itemDetails.fields.settings';

  public render() {
    const item = this.props.storage.item;
    const cache = {};

    const cells: Array<Array<string | ElementChild>> = [
      [
        <Text id={UserDetails.FieldIdentifier} />,
        item.identifier,
      ],
      [
        <Text id={UserDetails.FieldCreated} />,
        format(item.created, 'DD MMM YYYY'),
      ],
      [
        <Text id={UserDetails.FieldCreatedBy} />,
        searchOrHitCache(this.props.storage.list, item.createdBy.id, cache).identifier,
      ],
      [
        <Text id={UserDetails.FieldLastAuthenticated} />,
        format(item.lastAuthenticated, 'DD MMM YYYY'),
      ],
      [
        <Text id={UserDetails.FieldRole} />,
        item.role.identifier,
      ],
      [
        <Text id={UserDetails.FieldSettings} />,
        <Lists.List items={Object.keys(item.settings).map((i) => {
          return {
          content: [
            <Text id={UserDetails.SettingsLanguageLabel} />,
            <Label margin='small-left'>{item.settings[i]}</Label>,
          ],
          };
        })} />,
      ],
    ];

    return <Containers.Div overflow='auto' width='1-1' animation={['fade']}>
      <Headings.H3 text={['top', 'emphasis']}
        padding='remove' margin='top' divider>
        { item.identifier }
      </Headings.H3>
      <Tables.Table>
        <Tables.Body>
          {
            cells.map((row) => <Tables.Row>
              {row.map((r, i) =>
                <Tables.Cell {...i === 0 ? { text: 'muted', padding: 'remove-left' } : {}}>{ r }</Tables.Cell>)}
            </Tables.Row>)
          }
        </Tables.Body>
      </Tables.Table>
    </Containers.Div>;
  }
}
