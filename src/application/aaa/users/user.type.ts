import { DocumentReference } from '../../../firebase';
import { IRole } from '../rights/role.type';

export interface IUser {
  created: number;
  createdBy: IUser;
  identifier: string;
  lastAuthenticated: number;
  role: IRole;
  settings: any;
  id: string;
}

export interface IUserRaw {
  created: number;
  createdBy: DocumentReference;
  identifier: string;
  lastAuthenticated: number;
  role: DocumentReference;
  settings: any;
  id: string;
}
