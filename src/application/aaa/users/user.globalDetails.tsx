import { isSameWeek, isToday } from 'date-fns';
import { Component, h } from 'preact';

import { HighlightList } from '../../../core/highlight-list';
import { I18nStore, Text } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { Containers, Headings } from '../../../core/ui/src';
import { Charts, someColors } from '../../main/charts';
import { ISectionStorage } from '../../main/section/section.layout';
import { IUser } from './user.type';

interface IUserGlobalDetailsProps {
  storage: ISectionStorage<IUser>;
  i18n: I18nStore;
  route: Route;
}

export class UserGlobalDetails extends Component<IUserGlobalDetailsProps, any> {
  private static DetailsLabel = 'aaa.users.details.label';
  private static HighlightsLabels = 'aaa.users.details.highlights.';

  public render() {
    const users = this.props.storage.list || [];
    let activeToday = 0;
    let activeLast7 = 0;
    const all = users.length;

    // Determine the statistics
    users.forEach((u) => {
      if (isToday(u.lastAuthenticated)) {
        activeToday += 1;
      } else if (isSameWeek(u.lastAuthenticated, new Date())) {
        activeLast7 += 1;
      }
    });

    const d = [activeToday, activeLast7, all - activeToday - activeLast7];
    const colors = someColors(5);
    const i18n = this.props.i18n as I18nStore;
    const labels = [
      i18n.translate('aaa.users.details.chart.active'),
      i18n.translate('aaa.users.details.chart.last7'),
      i18n.translate('aaa.users.details.chart.inactive'),
    ];

    const data = {
      datasets: [{
        backgroundColor: [
          colors[0],
          colors[3],
          colors[4],
        ],
        borderColor:
          new Array(d.length).fill('#222222'),
        borderWidth: 3,
        data: d,
      }],
      labels,
    };

    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
          <Text id={UserGlobalDetails.DetailsLabel} fields={{ label: this.props.route.label }} />
        </Headings.H3>
      <HighlightList items={[
        {
          details: <Text id={`${UserGlobalDetails.HighlightsLabels}active`} />,
          main: activeToday.toString(),
        },
        {
          details: <Text id={`${UserGlobalDetails.HighlightsLabels}last7`} />,
          main: activeLast7.toString(),
        },
        {
          details: <Text id={`${UserGlobalDetails.HighlightsLabels}registered`} />,
          main: all.toString(),
        },
      ]} />

      <hr />

      <Containers.Div padding={['large', 'remove-top']} margin='large-top'>
        <Charts.Doughnut data={data} width={400} height={400} options={{
          animation: { animateRotate: true, animateScale: true }, legend: { display: false } }} />
      </Containers.Div>

    </Containers.Div>;
  }
}
