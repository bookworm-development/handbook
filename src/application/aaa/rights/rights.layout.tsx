import { format, startOfDay } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../../core/grid';
import { I18nStore, Text } from '../../../core/intl';
import { action, computed, inject, observer } from '../../../core/store';
import { ILabelProps, Label } from '../../../core/ui/src/label/label';
import { DocumentReference, firebaseDB, listaAllOfCollection, resolveReference } from '../../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../../main/section/section.layout';
import { IUser } from '../users';
import { IRole } from './';
import { RightsCreate } from './rights.create';
import { RightsGlobalDetails } from './rights.globalDetails';
import { RightsDetails } from './rights.itemDetails';
import { KnownSectionActions, KnownSections } from './role.type';

interface IRightsLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

const MaxRightsAllowed = KnownSections.length * KnownSectionActions.length;

@inject('search', 'i18n', 'auth')
@observer
export class RightsLayout extends Section<IRole, IRightsLayoutProps, any> {
  private static ColumnsLabels = 'aaa.rights.grid.columns.';

  protected routePath = ['settings', 'rights'];
  protected collection = 'roles';

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: this.createLayoutConfig,
      global: this.globalLayoutConfig,
      item: this.itemLayoutConfig,
    };
  }

  protected async fetchListData(): Promise<IRole[]> {
    return await listaAllOfCollection<IRole>('roles');
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<IRole>> {
    return [
      {
        field: 'identifier',
        label: <Text id={`${RightsLayout.ColumnsLabels}1`} />,
        options: {
          expand: true,
        },
        render: this.renderField,
      },
      {
        field: 'created',
        label: <Text id={`${RightsLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'allow',
        label: <Text id={`${RightsLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<IRole>> {
    return [
      {
        field: 'identifier',
        order: 'asc',
      },
      {
        field: 'created',
        order: 'desc',
        valueFn: (created: number) => startOfDay(created).getTime(),
      },
      {
        field: 'allow',
        order: 'desc',
        valueFn: (allow: IRole['allow']) => computePermissionsCoverage(allow),
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<IRole>>> {
    return [
      [
        {
          field: 'identifier',
          term: this.term,
        },
      ],
    ];
  }

  @computed
  private get createLayoutConfig() {
    const layout = <RightsCreate i18n={this.props.i18n as I18nStore}
      traps={{
        submitBtn: {
          after: action((role: IRole) => {
            this.storage.list.push(role);
            this.globalDashboard();
          }),
        },
      }} />;

    return {
      layout,
    };
  }

  @computed
  private get globalLayoutConfig() {
    return {
      fetch: async () => await listaAllOfCollection<IUser>('/users'),
      layout: (users: IUser[]) =>
        <RightsGlobalDetails i18n={this.props.i18n as I18nStore}
          route={this.route} storage={this.storage} users={users} />,
    };
  }

  @computed
  private get itemLayoutConfig() {
    const layout = <RightsDetails i18n={this.props.i18n as I18nStore} route={this.route} storage={this.storage}
      traps={{ submitBtn: { after: action((role: IRole) => {
        // Update only the affected item inplace
        this.storage.list.forEach((r, i) => {
          if (role.id === r.id) {
            this.storage.list[i] = role;
          }
        });
        this.globalDashboard();
      }),
    } }} />;

    return {
      fetch: async () => {
        const item = this.storage.item;

        // Resolve the created by reference if needed
        if (item.createdBy && (item.createdBy as unknown as DocumentReference).get) {
          item.createdBy = await resolveReference<IUser>(item.createdBy as unknown as DocumentReference);
        }

        // Resolve the updated by reference if neede
        if (item.updatedBy) {
          if ((item.updatedBy as unknown as DocumentReference).get) {
            item.updatedBy = await resolveReference<IUser>(item.updatedBy as unknown as DocumentReference);
          } else if (typeof item.updatedBy === 'string') {
            item.updatedBy = await resolveReference<IUser>(firebaseDB.doc(item.updatedBy as string));
          }
        }

        console.info('should have all data resolved at this point', {...item});
      },
      layout,
    };
  }

  private renderField(value: any, key: keyof IRole) {
    switch (key) {
      case 'identifier':
        return value.toString();

      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      case 'allow':
        const coverage = computePermissionsCoverage(value as IRole['allow']);
        const attributes: ILabelProps = {};

        if (coverage >= 90) {
          attributes.styleType = 'danger';
        } else if (coverage < 90 && coverage >= 50) {
          attributes.styleType = 'warning';
        } else if (coverage <= 20) {
          attributes.styleType = 'success';
        } else {
          attributes.style = { background: 'grey' };
        }

        return <Label {...attributes}>{Math.floor(coverage)}%</Label>;

      default:
        return null;
    }
  }
}

/**
 * Given an allow map computes the permissions coverage
 */
export function computePermissionsCoverage(allow: IRole['allow']): number {
  let allowed = 0;

  KnownSections.forEach((s) => {
    const sectionAllows = allow[s];

    if (sectionAllows) {
      KnownSectionActions.forEach((a) => {
        if (sectionAllows[a]) {
          allowed += 1;
        }
      });
    }
  });

  return allowed / MaxRightsAllowed * 100;
}
