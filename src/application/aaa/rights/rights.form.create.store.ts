import { action, BaseStore } from '../../../core/store';
import { AuthStore } from '../auth';
import { IUser } from '../users';
import { RightsStore } from './rights.form.store';

export class CreateRightsStore extends RightsStore {
  @action
  public resetFields() {
    super.resetFields();

    this.values.createdBy = {
      loading: false,
      value: ((BaseStore.getInstane().getStore('auth') as AuthStore).user || {} as IUser),
    };
  }
}
