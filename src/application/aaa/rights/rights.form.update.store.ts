import { action } from '../../../core/store';
import { RightsStore } from './rights.form.store';
import { IRole, ISectionActions, KnownSectionActions, KnownSections } from './role.type';

export class UpdateRightsStore extends RightsStore {
  @action
  public resetFields() {
    super.resetFields();

    if (this.initialValues) {
      (Object.keys(this.initialValues) as Array<keyof IRole>).forEach((k) => {
        if (k === 'allow') {
          const allow = {} as Partial<IRole['allow']>;
          const original = this.initialValues.allow as IRole['allow'];

          KnownSections.forEach((s) => {
            allow[s] = allow[s] || this.emptySection;

            if (original[s]) {
              KnownSectionActions.forEach((a) => {
                (allow[s] as ISectionActions)[a] = a in original[s] ? original[s][a] : (allow[s] as ISectionActions)[a];
              });
            }
          });

          this.values.allow.value = allow as IRole['allow'];
        } else {
          (this.values as any)[k].value = (this.initialValues as any)[k];
        }
      });
    }
  }

  private get emptySection(): ISectionActions {
    const s = {} as Partial<ISectionActions>;

    KnownSectionActions.forEach((a) => s[a] = false);

    return s as ISectionActions;
  }
}
