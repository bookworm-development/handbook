import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../../core/forms/form.custom';
import { I18nStore, Text } from '../../../core/intl';
import { Containers, Headings } from '../../../core/ui/src';
import { RightsForm } from './rights.form';
import { CreateRightsStore } from './rights.form.create.store';

interface IRightsCreateProps {
  i18n: I18nStore;
  traps?: ICustomFormActionTraps;
}

export class RightsCreate extends Component<IRightsCreateProps, any> {
  private static CreateLabel = 'aaa.rights.create.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
        <Text id={RightsCreate.CreateLabel}
          fields={{ label: (this.props.i18n as I18nStore).translate('routes.rights.singular') }} />
      </Headings.H3>
      <RightsForm layout='horizontal' action='create' overwrite={new CreateRightsStore()}
        actionTraps={this.props.traps} />
    </Containers.Div>;
  }
}
