import { Component, h } from 'preact';

import { HighlightList } from '../../../core/highlight-list';
import { I18nStore, Text } from '../../../core/intl';
import { Route } from '../../../core/routing';
import { computed, observer } from '../../../core/store';
import { Containers, Headings } from '../../../core/ui/src';
import { Charts, someColors } from '../../main/charts';
import { ISectionStorage } from '../../main/section/section.layout';
import { IUser } from '../users';
import { computePermissionsCoverage } from './rights.layout';
import { IRole } from './role.type';

interface IRightsGlobalDetailsProps {
  storage: ISectionStorage<IRole>;
  i18n: I18nStore;
  route: Route;
  users: IUser[];
}

type RoleImpact = 'total' | 'high' | 'medium' | 'low';

const defaultColors = someColors(5);

const ImpactOrder: RoleImpact[] = ['total', 'high', 'medium', 'low'];
const ImpactDefaults: {
  [i in RoleImpact]: {
    highlight: { sign: string; impact: number; text: any; };
    color: string;
  }} = {
  high: {
    color: defaultColors[4],
    highlight: { sign: '>', impact: 50, text: 'warning' },
  },
  low: {
    color: defaultColors[3],
    highlight: { sign: '<', impact: 20, text: 'success' },
  },
  medium: {
    color: 'grey',
    highlight: { sign: '<', impact: 50, text: 'primary' },
  },
  total: {
    color: defaultColors[1],
    highlight: { sign: '>', impact: 90, text: 'danger' },
  },
};

@observer
export class RightsGlobalDetails extends Component<IRightsGlobalDetailsProps, any> {
  private static DetailsLabel = 'aaa.rights.details.label';
  private static ChartTitle = 'aaa.rights.details.chart.title';
  private static HighlightLabel = 'aaa.rights.details.highlights.label';

  private roleCoverageCache: { [roleId: string]: RoleImpact; } = {};

  public render() {
    const coverages = this.coverages;

    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
          <Text id={RightsGlobalDetails.DetailsLabel}
            fields={{ label: this.props.i18n.translate('routes.rights.plural') }} />
        </Headings.H3>
      <HighlightList options={{ width: 'child-1-4' }} items={ImpactOrder.map((t) =>
        ({
            details: <Containers.Div userSelect='none'>
              {ImpactDefaults[t].highlight.sign}<b>{ImpactDefaults[t].highlight.impact}</b>%
              <Containers.Div userSelect='none'>
                <Text id={RightsGlobalDetails.HighlightLabel} />
              </Containers.Div>
            </Containers.Div>,

            main: <Containers.Div text={ImpactDefaults[t].highlight.text}
              userSelect='none'>{coverages[t]}</Containers.Div>,
          }))} />

      <hr />

      <Containers.Div padding={['large', 'remove-top']} margin='large-top'>
        <Charts.PolarArea data={this.chartData} width={400} height={400} options={{
            animation: { animateRotate: true, animateScale: true },
            legend: { display: true, position: 'bottom' },
            title: {
              display: true,
              position: 'bottom',
              text: this.props.i18n.translate(RightsGlobalDetails.ChartTitle),
            },
          }} />
      </Containers.Div>

    </Containers.Div>;
  }

  @computed
  private get chartData() {
    const roles = this.props.storage.list;
    const rolesHash: { [roleId: string]: IRole } = {};
    const usersByRole:
      { [impact in RoleImpact]: { [roleId: string]: number; } } = { total: {}, high: {}, medium: {}, low: {} };

    roles.forEach((r) => {
      const impact = this.determineCoverageCategory(r);

      usersByRole[impact] = usersByRole[impact] || {};
      usersByRole[impact][r.id] = 0;

      rolesHash[r.id] = r;
    });

    this.props.users.forEach((u) => {
      // Sort users by roles
      const impact = this.roleCoverageCache[u.role.id];
      if (usersByRole[impact]) {
        usersByRole[impact][u.role.id] += 1;
      }
    });

    const data: number[] = [];
    const colors: string[] = [];
    const labels: string[] = [];

    // Prepare data, colors and labels
    (['total', 'high', 'medium', 'low'] as RoleImpact[]).forEach((t) => {
      const group = usersByRole[t];

      Object.keys(group).forEach((id) => {
        const ammount = group[id];
        const role = rolesHash[id];

        data.push(ammount);
        colors.push(ImpactDefaults[t].color);
        labels.push(role.identifier);
      });
    });

    return {
      datasets: [
        {
          backgroundColor: colors,
          borderColor:
            new Array(data.length).fill('#222222'),
          borderWidth: 3,
          data,
        },
      ],
      labels,
    };
  }

  @computed
  private get coverages() {
    const roles = this.props.storage.list;
    const coverages: { [impact in RoleImpact]: number } = {
      high: 0,
      low: 0,
      medium: 0,
      total: 0,
    };

    // Compute the coverages hash
    roles.map((r) => coverages[this.determineCoverageCategory(r)] += 1);

    return coverages;
  }

  private determineCoverageCategory(role: IRole): RoleImpact {
    if (this.roleCoverageCache[role.id]) {
      return this.roleCoverageCache[role.id];
    }

    const coverage = computePermissionsCoverage(role.allow);
    let impact: RoleImpact = 'medium';

    if (coverage >= 90) {
      impact = 'total';
    } else if (coverage < 90 && coverage >= 50) {
      impact = 'high';
    } else if (coverage <= 20) {
      impact = 'low';
    }

    this.roleCoverageCache[role.id] = impact;

    return impact;
  }
}
