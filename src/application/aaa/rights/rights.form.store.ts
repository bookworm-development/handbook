import { format } from 'date-fns';

import { I18nStore } from '../../../core/intl';
import { action, BaseStore, computed, observable, runInAction } from '../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../core/store/form';
import { Notifications } from '../../../core/ui/src';
import { asReferenceString, firebaseDB } from '../../../firebase';
import { AuthStore } from '../auth';
import { IUser } from '../users';
import { IRole, ISectionActions, KnownSectionActions, KnownSections } from './role.type';

export class RightsStore extends FormStore<IRole> {
  private static DateFormat = 'DD MMM YYYY HH:mm';

  protected validationRules: IFieldsValidationRules = {
    identifier: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 2 },
    },
  };

  protected transformersOut = {
    created: (val: any) => format(val, RightsStore.DateFormat) as any,
    createdBy: (val: any) => val.identifier,
    updated: (val: any) => val && format(val, RightsStore.DateFormat) || '',
    updatedBy: (val: any) => val && val.identifier || '',
  };

  @observable
  private progress: boolean;

  @action
  public resetFields() {
    this.values = {
      allow: {
        loading: false,
        value: this.defaultAllow,
      },
      created: {
        loading: false,
        value: Date.now(),
      },
      createdBy: {
        loading: false,
        value: ({}) as IUser,
      },
      id: {
        loading: false,
        value: '',
      },
      identifier: {
        loading: false,
        value: '',
      },
      updated: {
        loading: false,
        value: null,
      },
      updatedBy: {
        loading: false,
        value: null,
      },
    };
  }

  @computed
  public get submitInProgress() {
    return this.progress;
  }

  public get defaultAllow() {
    const obj: Partial<IRole['allow']> = {};
    KnownSections.forEach((k) => {
      const actionsObj: ISectionActions = {} as any;

      KnownSectionActions.forEach((a) => actionsObj[a] = false);

      obj[k] = actionsObj;
    });

    return obj as IRole['allow'];
  }

  @action.bound
  public async create(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    // Create a new role using the available data
    const rawRole = {
      allow: this.values.allow.value,
      created: this.values.created.value,
      createdBy: asReferenceString('users', this.values.createdBy.value.id),
      identifier: this.values.identifier.value,
    };

    this.progress = true;

    const roleRef = await firebaseDB.collection('roles').add(rawRole);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'roles',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'aaa.rights.notifications.created',
          { identifier: rawRole.identifier },
        ),
        status: 'success' });
    });

    const role = await roleRef.get();
    return {
      ...role.data(),
      id: roleRef.id,
    };
  }

  @action.bound
  public async update(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    this.progress = true;

    // Get the current role's reference
    const roleRef = await firebaseDB.collection('roles').doc(this.values.id.value);
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;
    const author = auth.user as IUser;

    // Update the role with the form's content
    await roleRef.set({
      allow: this.values.allow.value,
      identifier: this.values.identifier.value,
      updated: Date.now(),
      updatedBy: asReferenceString('users', author.id),
    }, { merge: true });

    const roleSnapshot = await roleRef.get();
    const role = {
      ...roleSnapshot.data(),
      id: roleRef.id,
    } as IRole;

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'roles',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'aaa.rights.notifications.updated',
          { identifier: role.identifier },
        ),
        status: 'success' });
    });

    return role;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(RightsStore);
