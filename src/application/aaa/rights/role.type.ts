import { DocumentReference } from '../../../firebase';
import { IUser } from '../users/user.type';

export interface IRole {
  allow: AllowRules;
  created: number;
  createdBy: IUser;
  updated: number | null;
  updatedBy: IUser | null;
  identifier: string;
  id: string;
}

export type Sections =
  // Top level items
  'main' | 'clients' | 'statistics' | 'products' | 'barcodes' | '/logout' |

  // Stocks items
  'stocks' |
  'activity' | 'stock' | 'transaction' |

  // Settings items
  'settings' |
  'users' | 'rights' | 'limits' | 'alerts';

export type SectionActions = keyof ISectionActions;

export const KnownSections: Sections[] = [
  // Top level items
  'main', 'clients', 'statistics', 'products', 'barcodes', '/logout',

  // Stocks items
  'stocks',
  'activity', 'stock', 'transaction',

  // Settings items
  'settings',
  'users', 'rights', 'limits', 'alerts',
];
export const KnownSectionActions: SectionActions[] = ['create', 'delete', 'get', 'list', 'update'];

export interface ISectionActions  {
  create: boolean;
  delete: boolean;
  get: boolean;
  list: boolean;
  update: boolean;
}

type AllowRules = {
  [s in Sections]: ISectionActions;
};

export interface IRoleRaw {
  allow: AllowRules;
  created: number;
  createdBy: DocumentReference;
  updated: number;
  updatedBy: DocumentReference;
  identifier: string;
  id: string;
}
