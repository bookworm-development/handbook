import { CustomForm, Field, h, ICustomFormActions, ICustomFormProps, Inputs } from '../../../core/forms';
import { Text } from '../../../core/intl';
import { computed, inject, observer } from '../../../core/store';
import { Containers } from '../../../core/ui/src';
import { Description, IItem } from '../../../core/ui/src/lists/list.description';
import { RightsStore } from './rights.form.store';
import './rights.form.store';
import { IRole, KnownSectionActions, KnownSections } from './role.type';

interface IRightsFormProps extends ICustomFormProps<IRole> {
  store: RightsStore;
  action: 'create' | 'update' | 'view';
}

@inject((stores) => ({
  store: stores.rights,
}))
@observer
export class RightsForm extends CustomForm<IRole, Partial<IRightsFormProps>, any> {
  private static CreateBtnLabel = 'aaa.rights.form.submitBtnCreate';
  private static UpdateBtnLabel = 'aaa.rights.form.submitBtnUpdate';
  private static FieldCreated = 'aaa.rights.form.created';
  private static FieldCreatedBy = 'aaa.rights.form.createdBy';
  private static FieldUpdated = 'aaa.rights.form.updated';
  private static FieldUpdatedBy = 'aaa.rights.form.updatedBy';
  private static FieldIdentifier = 'aaa.rights.form.identifier';
  private static FieldAllow = 'aaa.rights.form.allow';

  @computed
  protected get actions() {
    if (this.props.action === 'create') {
      return {
        submitBtn: {
          handler: this.store.create,
          label: <Text id={RightsForm.CreateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    } else if (this.props.action === 'update') {
      return {
        submitBtn: {
          handler: this.store.update,
          label: <Text id={RightsForm.UpdateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    }

    return {
      resetBtn: null,
      submitBtn: null,
    } as ICustomFormActions;
  }

  @computed
  protected get fields() {
    const updated = this.store.fieldMeta('updated').value.get();

    return [
      // Identifier
      <Field label={<Text id={RightsForm.FieldIdentifier} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
          { ...this.store.fieldMeta('identifier') } />
      </Field>,

      // Created
      <Field label={<Text id={RightsForm.FieldCreated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('created') } />
      </Field>,

      // CreatedBy
      <Field label={<Text id={RightsForm.FieldCreatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('createdBy') } />
      </Field>,

      ...updated ? [
      // Updated
      <Field label={<Text id={RightsForm.FieldUpdated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('updated') } />
      </Field>,

      // UpdateddBy
      <Field label={<Text id={RightsForm.FieldUpdatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('updatedBy') } />
      </Field>,
      ] : [],

      // Allow
      <Field label={<Text id={RightsForm.FieldAllow} />}>
        <Description items={this.generateAllowSections} padding={['small', 'remove-left']} />
      </Field>,
    ];
  }

  private get generateAllowSections(): IItem[] {
    return KnownSections.map((s) => {
      return {
        description: KnownSectionActions.map((a) => {
          return <Inputs.Checkbox {...this.store.embededFieldMeta(['allow', s, a])} label={a} margin='small-right' />;
        }),
        term: <Containers.Div text='emphasis'>{s}</Containers.Div>,
      } as IItem;
    }) as IItem[];
  }
}
