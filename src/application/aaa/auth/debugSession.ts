import { deepClone } from '../../../core/util';
import {
  IRole,
  ISectionActions,
  KnownSectionActions,
  KnownSections,
  Sections,
} from '../rights/role.type';
import { IUser } from '../users';
import { ISession } from './auth.store';

import * as token from './debugSession.token.json';

const currentUser = {
  created: 1549552557770,
  createdBy: {},
  id: 'OfkHJm5NTUax31WqFuR5lTAKTpT2',
  identifier: 'ghepesdoru@gmail.com',
  lastAuthenticated: 1550583993266,
  role: {
    allow: KnownSections.reduce((ret, s) => {
      ret[s] = KnownSectionActions.reduce((ret2, a) => {
        ret2[a] = true;

        return ret2;
      }, {} as any);

      return ret;
    }, {} as { [section in Sections]?: ISectionActions; }) as IRole['allow'],
    created: 1549552557770,
    createdBy: {},
    id: 'Dc93jBzQYcYXwKJkESCA',
    identifier: 'administrator',
    updated: null,
    updatedBy: null,
  } as IRole,
  settings: {
    locale: 'ro',
  },
} as IUser;

// Fix extra roles
currentUser.role.allow.activity.create = false;
currentUser.role.allow.activity.delete = false;
currentUser.role.allow.activity.get = false;
currentUser.role.allow.activity.update = false;
currentUser.role.allow.stocks.delete = false;
currentUser.role.allow.stock.delete = false;
currentUser.role.allow.transaction.delete = false;


export const debugSession = {
  currentUser,
  period: {
    started: new Date(),
  },
  role: {
    allow: deepClone(currentUser.role.allow),
    identifier: deepClone(currentUser.role.identifier),
  },
  token: token.token,
  user: {
    displayName: currentUser.identifier,
    email: currentUser.identifier,
    phoneNumber: '',
    photoURL: '',
    providerId: '',
    uid: currentUser.id,
  },
} as ISession;
