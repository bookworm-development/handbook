import { CustomForm, Field, h, Icons, ICustomFormActions, ICustomFormProps, Inputs } from '../../../../core/forms';
import { Text } from '../../../../core/intl';
import { computed, inject, observer } from '../../../../core/store';
import { IFormStoreFields, LoginStore } from './login.form.store';
import './login.form.store';

interface ILoginFormProps extends ICustomFormProps<IFormStoreFields> {
  store: LoginStore;
}

@inject((stores) => ({
  store: stores.login,
}))
@observer
export class LoginForm extends CustomForm<IFormStoreFields, Partial<ILoginFormProps>, any> {
  private static ResetBtnLabelId = 'aaa.auth.login.actions.reset';
  private static SubmitBtnLabelId = 'aaa.auth.login.actions.submit';
  private static MailFieldLabelId = 'aaa.auth.login.email';
  private static PassFieldLabelId = 'aaa.auth.login.password';

  @computed
  protected get actions(): ICustomFormActions {
     return {
      group: true,
      resetBtn: {
        disabled: !this.store.resetAction,
        handler: this.store.reset,
        label: <Text id={LoginForm.ResetBtnLabelId} />,
      },
      submitBtn: {
        disabled: !this.store.submitAction,
        handler: this.store.login,
        label: <Text id={LoginForm.SubmitBtnLabelId} />,
        loading: this.store.submitInProgress,
      },
    };
  }

  @computed
  protected get fields() {
    return [
      <Field label={<Text id={LoginForm.MailFieldLabelId} />}>
        <Inputs.Input autocomplete='off' width={'1-1'}
          { ...this.store.fieldMeta('email') } icon={Icons.app.mail} iconAlign='right' />
      </Field>,
      <Field label={<Text id={LoginForm.PassFieldLabelId} />}>
        <Inputs.SensitiveInput autocomplete='off'
          { ...this.store.fieldMeta('password') } icon={Icons.app.lock} iconAlign='right' />
      </Field>,
    ];
  }
}
