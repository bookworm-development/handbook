import { action, BaseStore, computed } from '../../../../core/store';
import { FormStore, IFieldsValidationRules } from '../../../../core/store/form';
import { Notifications } from '../../../../core/ui/src';
import { AuthStore } from '../auth.store';

export interface IFormStoreFields {
  email: string;
  password: string;
}

export class LoginStore extends FormStore<IFormStoreFields> {
  protected validationRules: IFieldsValidationRules = {
    email: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 5 },
    },
    password: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 5 },
    },
  };

  @action
  public resetFields() {
    this.values = {
      email: {
        loading: false,
        value: '',
      },
      password: {
        loading: false,
        value: '',
      },
    };
  }

  @action.bound
  public async login(ev: MouseEvent) {
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;

    ev.stopPropagation();
    ev.preventDefault();

    const result = await auth.login(this.values.email.value, this.values.password.value);

    if (!result || result instanceof Error) {
      this.values.email.validity = { valid: false };
      this.values.password.validity = { valid: false };

      if (result) {
        this.values.email.validity.reason = result.message;
        this.values.password.validity.reason = result.message;

        Notifications.new({ group: 'auth', message: result.message, status: 'danger' });
      }
    }
  }

  @computed
  public get submitInProgress() {
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;

    return auth.progress;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(LoginStore);
