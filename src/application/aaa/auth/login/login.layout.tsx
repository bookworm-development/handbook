import { Component, h } from 'preact';

import { Text } from '../../../../core/intl';
import { observer } from '../../../../core/store';
import { Containers, Headings, Icons, Link, Logo } from '../../../../core/ui/src';
import { LoginForm } from './login.form';

import * as background from '../../../../assets/background.jpg';

@observer
export class LoginLayout extends Component<any, any> {
  public render() {
    return <Containers.Grid height='viewport' animation={['fade', 'slide-right-medium']}>
      <Containers.Grid size='collapse' background='secondary' height='1-1' width='1-3' boxShadow='large'>
        <Containers.Panel backgroundImage={background} background={['cover', 'center-right']} width='1-1' />
      </Containers.Grid>

      <Containers.Section height='viewport' flex={['center', 'middle']} width='2-3'>
        <Containers.Grid size='collapse' width={['2-3', 'child-1-1']}>
          <Logo margin='medium-bottom' location='/'>
            <Icons.Icon icon={Icons.storage['file-text']} margin={['small-bottom']} padding='remove' ratio={4} />
            <Headings.H1 width='1-1' margin={'remove-top'}>
              <Text id={'application.title'} />
            </Headings.H1>
          </Logo>

          <LoginForm />

          <Containers.Panel margin='large-top' text={['center', 'meta', 'emphasis']}>
            &copy; 2019 <Link label={'Bookworm Development SRL'} styleType='heading' />
            <Containers.Div margin='remove' text={['small', 'muted']}>
              All Rights Reserved
            </Containers.Div>
          </Containers.Panel>
        </Containers.Grid>
      </Containers.Section>
    </Containers.Grid>;
  }
}
