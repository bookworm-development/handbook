import { User, UserInfo } from 'firebase';
import { route } from 'preact-router';

import { Route } from '../../../core/routing';
import { action, BaseStore, computed, observable, runInAction, Store} from '../../../core/store';
import { DocumentReference, firebaseApp, firebaseDB } from '../../../firebase';
import { routes } from '../../application.routes';
import { IRole } from '../rights';
import { SectionActions, Sections } from '../rights/role.type';
import { IUser } from '../users';

import { debugSession } from './debugSession'; // TODO: Remove at the end

export class AuthStore extends Store {
  @observable private session: ISession;

  @observable private inProgress: boolean = false;

  public constructor() {
    super();

    // Reset the store on initialization
    this.reset();
  }

  public get user() {
    return this.session.currentUser;
  }

  /**
   * Resets the store to it's original blank state
   */
  public reset() {
    // Enforce the debug sessionf or debug/development
    this.session = debugSession;

    // this.session = {
    //   currentUser: null,
    //   period: {},
    //   role: {
    //     allow: {} as any,
    //     identifier: 'anonymous',
    //   },
    //   user: this.extractUserInfo({}),
    // };
  }

  @computed
  public get progress() {
    return this.inProgress;
  }

  /**
   * Checks if an actively logged in session exists
   */
  @computed
  public get loggedIn(): boolean {
    return !!(
        this.session.period && this.session.period.started && !this.session.period.ended &&
        this.session.token && this.session.user.uid
      );
  }

  @action.bound
  public async login(email: string, password: string): Promise<Error | boolean> {
    this.inProgress = true;

    try {
      const loggedIn = await firebaseApp.auth().signInWithEmailAndPassword(email, password);

      if (loggedIn.user && (loggedIn.user as User).uid) {
        runInAction(async () => {
          const u = loggedIn.user as User;

          this.session.user = this.extractUserInfo(u);
          this.session.period.started = new Date();
          this.session.token = u.refreshToken;

          // Update the last logged in time for this user
          await this.markAuthenticatedUser();

          // Fetch the user rights for the currently logged in user.
          const fetchedRights = await this.fetchUserRights();

          if (fetchedRights && (fetchedRights as Error).message) {
            runInAction(() => { this.inProgress = false; });

            return fetchedRights as Error;
          }

          // Route to the main page
          route(routes.routes[0].route.path, true);
        });
      } else {
        runInAction(() => { this.inProgress = false; });

        return false;
      }
    } catch (e) {
      runInAction(() => { this.inProgress = false; });

      return new Error(`${e.code}: ${e.message}`);
    }

    runInAction(() => { this.inProgress = false; });

    return true;
  }

  @action.bound
  public async logout(): Promise<boolean> {
    try {
      await firebaseApp.auth().signOut();

      this.session.period.ended = new Date();
    } catch (_) {
      return false;
    }

    return true;
  }

  public isRouteAvailable(r: Route) {
    if (!r.noAuthentication && !this.loggedIn) {
      // Do not bother with routes requiring authentication in unnauthenticated contexts
      return false;
    }

    // Check if the role allows listing of the route (as all main routes end in a listing)
    return this.roleAllowsTo(r, 'list');
  }

  public roleAllowsTo(r: Route, a: SectionActions): boolean {
    // TODO: in future projects change the section check to use the absolute path and allow for nested
    // rights declarations
    const rights = this.session.role.allow[r.path as Sections];

    if (rights) {
      return !!rights[a];
    }

    return false;
  }

  @action
  private async fetchUserRights() {
    // Grab user rights
    try {
      const userDetails = await firebaseDB.doc(`users/${this.session.user.uid}`).get();
      this.session.currentUser = userDetails.data() as IUser;

      if (userDetails.exists) {
        const userData = await userDetails.data() as IUser & { role: DocumentReference };

        const role = await userData.role.get();

        if (role && role.exists) {
          runInAction(() => {
            const roleData = role.data();

            if (roleData) {
              this.session.role.identifier = roleData.identifier;
              this.session.role.allow = roleData.allow;
            }
          });
        }
      }
    } catch (e) {
      return e;
    }

    return true;
  }

  @action
  private async markAuthenticatedUser() {
    const user = firebaseDB.doc(`users/${this.session.user.uid}`);

    await user.update({
      lastAuthenticated: Date.now(),
    });
  }

  /**
   * Given a Firebase User or plain object, extracts the Firebase UserInfo equivalent
   */
  private extractUserInfo(user: User | {}): UserInfo {
    if ((user as User).uid) {
      const u = user as User;

      return {
        displayName: u.displayName,
        email: u.email,
        phoneNumber: u.phoneNumber,
        photoURL: u.photoURL,
        providerId: u.providerId,
        uid: u.uid,
      };
    }

    return {
      displayName: '',
      email: '',
      phoneNumber: '',
      photoURL: '',
      providerId: '',
      uid: '',
    };
  }
}

export interface ISession {
  period: { started?: Date; ended?: Date; };
  role: ISessionRole;
  user: UserInfo;
  currentUser: IUser | null;
  token?: string;
}

interface ISessionRole {
  allow: IRole['allow'];
  identifier: IRole['identifier'];
}

// Register an initial instance of the store
BaseStore.registerStoreClass(AuthStore);
