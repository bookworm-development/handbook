import { Component, h } from 'preact';
import { IntlProvider } from 'preact-i18n';

import { I18nStore } from '../core/intl';
import { inject, observer } from '../core/store';
import { Containers } from '../core/ui/src';
import { ApplicationRouter } from './application.router';

interface IApplicationI18NProps {
  i18n: I18nStore;
}

@inject('i18n')
@observer
export class ApplicationIntl extends Component<IApplicationI18NProps, any> {
  public render() {
    const intl = this.props.i18n;

    return intl.loading ?
      <Containers.Tile loading={true} styleType='muted' height='viewport' flex={['center', 'middle']} /> :
      <IntlProvider definition={intl.definition} scope={intl.scope}>
        <ApplicationRouter />
      </IntlProvider>;
  }
}
