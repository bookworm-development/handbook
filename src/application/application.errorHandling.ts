import { onError } from 'mobx-preact';
import * as UIKit from 'uikit';

// Define a global error handler for component errors
onError((error: Error) => {
  UIKit.notification({
    message: (error || { message: ''}).message,
    status: 'danger',
  });
});
