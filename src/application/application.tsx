import { Component, h } from 'preact';

import { I18nStore } from '../core/intl/intl.store';
import { BaseStore, Provider } from '../core/store';
import { ApplicationIntl } from './application.intl';

import './application.scss';

export class Application extends Component<any, any> {
  public render() {
    const baseStore = BaseStore.getInstane();
    const i18n = new I18nStore();

    return <Provider baseStore={baseStore}>
      <ApplicationIntl i18n={i18n} />
    </Provider>;
  }
}

require('preact/debug');
