import { action, BaseStore } from '../../core/store';
import { AuthStore } from '../aaa/auth';
import { IUser } from '../aaa/users';
import { ClientMetaSortOrder, IClientMeta } from './client.type';
import { ClientsStore } from './clients.form.store';

export class CreateClientsStore extends ClientsStore {
  @action
  public resetFields() {
    super.resetFields();

    this.values.createdBy = {
      loading: false,
      value: ((BaseStore.getInstane().getStore('auth') as AuthStore).user || {} as IUser),
    };

    this.values.meta = {
      loading: false,
      value: Object.keys(ClientMetaSortOrder).reduce((r, v) => {
        r[v] = '';

        return r;
      }, {} as IClientMeta),
    };
  }
}
