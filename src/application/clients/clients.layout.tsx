import { format, startOfDay } from 'date-fns';
import { h } from 'preact';

import { ICategoryFilterRules, ICategorySetting, ICategorySortRules } from '../../core/grid';
import { I18nStore, Text } from '../../core/intl';
import { action, computed, inject, observer } from '../../core/store';
import { Label } from '../../core/ui/src';
import { listaAllOfCollection } from '../../firebase';
import { ContextualLayouts, ISectionProps, Section } from '../main/section/section.layout';
import { IClient, sortMeta } from './client.type';
import { ClientsCreate } from './clients.create';
import { ClientsGlobalDetails } from './clients.globalDetails';
import { ClientsDetails } from './clients.itemDetails';

interface IClientsLayoutProps extends ISectionProps {
  i18n?: I18nStore;
}

@inject('search', 'i18n', 'auth')
@observer
export class ClientsLayout extends Section<IClient, IClientsLayoutProps, any> {
  private static ColumnsLabels = 'clients.grid.columns.';

  protected routePath = ['clients'];
  protected collection = 'clients';

  @computed
  protected get layouts(): ContextualLayouts {
    return {
      create: this.createLayoutConfig,
      global: this.globalLayoutConfig,
      item: this.itemLayoutConfig,
    };
  }

  protected async fetchListData(): Promise<IClient[]> {
    return await listaAllOfCollection<IClient>('clients');
  }

  @computed
  protected get dataCategories(): Array<ICategorySetting<IClient>> {
    return [
      {
        field: 'identifier',
        label: <Text id={`${ClientsLayout.ColumnsLabels}1`} />,
        render: this.renderField,
      },
      {
        field: 'created',
        label: <Text id={`${ClientsLayout.ColumnsLabels}2`} />,
        render: this.renderField,
      },
      {
        field: 'meta',
        label: <Text id={`${ClientsLayout.ColumnsLabels}3`} />,
        render: this.renderField,
      },
    ];
  }

  @computed
  protected get sortingRules(): Array<ICategorySortRules<IClient>> {
    return [
      {
        field: 'identifier',
        order: 'asc',
      },
      {
        field: 'created',
        order: 'desc',
        valueFn: (created: number) => startOfDay(created).getTime(),
      },
    ];
  }

  @computed
  protected get filteringRules(): Array<Array<ICategoryFilterRules<IClient>>> {
    return [
      [
        {
          field: 'identifier',
          term: this.term,
        },
      ],
      [
        {
          field: 'id',
          term: this.term,
        },
      ],
    ];
  }

  @computed
  private get createLayoutConfig() {
    const layout = <ClientsCreate i18n={this.props.i18n as I18nStore}
      traps={{
        submitBtn: {
          after: action((client: IClient) => {
            this.storage.list.push(client);
            this.globalDashboard();
          }),
        },
      }} />;

    return {
      layout,
    };
  }

  @computed
  private get globalLayoutConfig() {
    return {
      // fetch: async () => await listaAllOfCollection<IProduct>('/products'),
      layout: <ClientsGlobalDetails i18n={this.props.i18n as I18nStore}
          route={this.route} storage={this.storage} />,
    };
  }

  @computed
  private get itemLayoutConfig() {
    return {
      layout: <ClientsDetails i18n={this.props.i18n as I18nStore} route={this.route} storage={this.storage}
        traps={{ submitBtn: { after: action((client: IClient) => {
          // Update only the affected item inplace
          this.storage.list.forEach((r, i) => {
            if (client.id === r.id) {
              this.storage.list[i] = client;
            }
          });
          this.globalDashboard();
        }),
      } }} />,
    };
  }

  private renderField(value: any, key: keyof IClient) {
    switch (key) {
      case 'created':
        return format(new Date(value), 'DD MMM YYYY');

      case 'identifier':
        return value;

      case 'meta':
        return Object.keys(value).sort(sortMeta).slice(0, 2).map((k) => {
          return <Label display='inline-block' margin='small-left'>{value[k]}</Label>;
        });

      default:
        return null;
    }
  }
}
