import { format } from 'date-fns';

import { I18nStore } from '../../core/intl';
import { action, BaseStore, computed, observable, runInAction } from '../../core/store';
import { FormStore, IFieldsValidationRules } from '../../core/store/form';
import { Notifications } from '../../core/ui/src';
import { asReferenceString, firebaseDB, resolveReference } from '../../firebase';
import { AuthStore } from '../aaa/auth';
import { IUser } from '../aaa/users';
import { IClient, IClientMeta } from './client.type';

export class ClientsStore extends FormStore<IClient> {
  private static DateFormat = 'DD MMM YYYY HH:mm';

  protected validationRules: IFieldsValidationRules = {
    identifier: {
      maxlength: { maxLength: 20 },
      minlength: { minLength: 3 },
    },
  };

  protected transformersOut = {
    created: (val: any) => format(val, ClientsStore.DateFormat) as any,
    createdBy: (val: any) => val.identifier,
    updated: (val: any) => val && format(val, ClientsStore.DateFormat) as any || '',
    updatedBy: (val: any) => val && val.identifier || '',
  };

  @observable
  private progress: boolean;

  @action
  public resetFields() {
    this.values = {
      created: {
        loading: false,
        value: Date.now(),
      },
      createdBy: {
        loading: false,
        value: ({}) as IUser,
      },
      id: {
        loading: false,
        value: '',
      },
      identifier: {
        loading: false,
        value: '',
      },
      meta: {
        loading: false,
        value: {} as IClientMeta,
      },
      updated: {
        loading: false,
        value: null,
      },
      updatedBy: {
        loading: false,
        value: null,
      },
    };
  }

  @computed
  public get submitInProgress() {
    return this.progress;
  }

  @action.bound
  public async create(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    // Create a new barcode using the available data
    const rawClient = {
      created: this.values.created.value,
      createdBy: asReferenceString('users', this.values.createdBy.value.id),
      identifier: this.values.identifier.value,
      meta: (this.values.meta || {value: {}}).value,
    };

    this.progress = true;

    const clientRef = await firebaseDB.collection('clients').add(rawClient);
    const client = await resolveReference<IClient>(clientRef);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'clients',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'clients.notifications.created',
          { identifier: client.identifier },
        ),
        status: 'success' });
    });

    return client;
  }

  @action.bound
  public async update(ev: MouseEvent) {
    ev.stopPropagation();
    ev.preventDefault();
    ev.stopImmediatePropagation();

    this.progress = true;

    // Get the current role's reference
    const clientRef = await firebaseDB.collection('clients').doc(this.values.id.value);
    const auth = BaseStore.getInstane().getStore('auth') as AuthStore;
    const author = auth.user as IUser;

    // Update the role with the form's content
    await clientRef.set({
      identifier: this.values.identifier.value,
      meta: this.values.meta.value,
      updated: Date.now(),
      updatedBy: asReferenceString('users', author.id),
    }, { merge: true });

    const client = await resolveReference<IClient>(clientRef);

    runInAction(() => {
      this.progress = false;

      Notifications.new({
        group: 'clients',
        message: (BaseStore.getInstane().getStore('i18n') as I18nStore).translate(
          'clients.notifications.updated',
          { identifier: client.identifier },
        ),
        status: 'success' });
    });

    return client;
  }
}

// Register an initial instance of the store
BaseStore.registerStoreClass(ClientsStore);
