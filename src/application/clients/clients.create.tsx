import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../core/forms/form.custom';
import { I18nStore, Text } from '../../core/intl';
import { Containers, Headings } from '../../core/ui/src';
import { ClientsForm } from './clients.form';
import { CreateClientsStore } from './clients.form.create.store';

interface IClientsCreateProps {
  i18n: I18nStore;
  traps?: ICustomFormActionTraps;
}

export class ClientsCreate extends Component<IClientsCreateProps, any> {
  private static CreateLabel = 'clients.create.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
        <Text id={ClientsCreate.CreateLabel}
          fields={{ label: (this.props.i18n as I18nStore).translate('routes.clients.singular') }} />
      </Headings.H3>
      <ClientsForm layout='horizontal' action='create' overwrite={new CreateClientsStore()}
        actionTraps={this.props.traps} />
    </Containers.Div>;
  }
}
