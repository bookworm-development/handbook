import { IUser } from '../aaa/users/user.type';

export interface IClient {
  created: number;
  createdBy: IUser;
  updated: number | null;
  updatedBy: IUser | null;
  id: string;
  identifier: string;
  meta: IClientMeta;
}

export interface IClientMeta {
  name?: string;
  surname?: string;
  address?: string;
  email?: string;
  phone?: string;
  [detail: string]: string | undefined;
}

export type ClientMetaTypes =
  'name' | 'surname' | 'address' | 'email' | 'phone';

export const ClientMetaSortOrder: { [p in ClientMetaTypes]: number; } = {
  address: 3,
  email: 2,
  name: 4,
  phone: 1,
  surname: 5,
};

export function sortMeta(a: string, b: string): number {
  const aa = ClientMetaSortOrder[a as ClientMetaTypes] || 6;
  const bb = ClientMetaSortOrder[b as ClientMetaTypes] || 6;

  return aa - bb;
}
