import { CustomForm, Field, h, ICustomFormActions, ICustomFormProps, Inputs } from '../../core/forms';
import { Text } from '../../core/intl';
import { computed, inject, observer } from '../../core/store';
import { ElementChildOnly, Headings, Lists } from '../../core/ui/src';
import { IItem } from '../../core/ui/src/lists/list';
import { IClient, sortMeta } from './client.type';
import { ClientsStore } from './clients.form.store';

import './clients.form.store';

interface IClientsFormProps extends ICustomFormProps<IClient> {
  store: ClientsStore;
  action: 'create' | 'update' | 'view';
}

@inject((stores) => ({
  store: stores.rights,
}))
@observer
export class ClientsForm extends CustomForm<IClient, Partial<IClientsFormProps>, any> {
  private static CreateBtnLabel = 'clients.form.submitBtnCreate';
  private static UpdateBtnLabel = 'clients.form.submitBtnUpdate';
  private static FieldCreated = 'clients.form.created';
  private static FieldCreatedBy = 'clients.form.createdBy';
  private static FieldUpdated = 'clients.form.updated';
  private static FieldUpdatedBy = 'clients.form.updatedBy';
  private static FieldIdentifier = 'clients.form.identifier';

  private static FieldMeta = 'clients.form.meta.label';
  private static FieldMetaEmpty = 'clients.form.meta.empty';
  private static FieldMetaName = 'clients.form.meta.name';
  private static FieldMetaSurname = 'clients.form.meta.surname';
  private static FieldMetaAddress = 'clients.form.meta.address';
  private static FieldMetaEmail = 'clients.form.meta.email';
  private static FieldMetaPhone = 'clients.form.meta.phone';
  private static FieldMetaUnknownField = 'clients.form.meta.anonymous';

  @computed
  protected get actions() {
    if (this.props.action === 'create') {
      return {
        submitBtn: {
          handler: this.store.create,
          label: <Text id={ClientsForm.CreateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    } else if (this.props.action === 'update') {
      return {
        submitBtn: {
          handler: this.store.update,
          label: <Text id={ClientsForm.UpdateBtnLabel} />,
          loading: this.store.submitInProgress,
        },
      } as ICustomFormActions;
    }

    return {
      resetBtn: null,
      submitBtn: null,
    } as ICustomFormActions;
  }

  @computed
  protected get fields() {
    const updated = this.store.fieldMeta('updated').value.get();
    const meta = this.generateMetaFields;

    return [
      // Identifier
      <Field label={<Text id={ClientsForm.FieldIdentifier} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
          { ...this.store.fieldMeta('identifier') } />
      </Field>,

      // Created
      <Field label={<Text id={ClientsForm.FieldCreated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('created') } />
      </Field>,

      // CreatedBy
      <Field label={<Text id={ClientsForm.FieldCreatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('createdBy') } />
      </Field>,

      ...updated ? [
      // Updated
      <Field label={<Text id={ClientsForm.FieldUpdated} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('updated') } />
      </Field>,

      // UpdateddBy
      <Field label={<Text id={ClientsForm.FieldUpdatedBy} />}>
        <Inputs.Input autocomplete='off' width={'1-1'} blank readonly margin='small-bottom'
          { ...this.store.fieldMeta('updatedBy') } />
      </Field>,
      ] : [],

      // Meta
      ...[
        <Headings.H4>
          <Text id={ClientsForm.FieldMeta} />
        </Headings.H4>,

        meta.length ?
          <Lists.List items={meta} /> :
          <Text id={ClientsForm.FieldMetaEmpty} />,
      ],
    ];
  }

  @computed
  private get generateMetaFields(): IItem[] {
    const meta = this.store.fieldMeta('meta').value.get();

    return Object.keys(meta).sort(sortMeta).map((k) => {
      const content: ElementChildOnly[] = [];

      switch (k) {
        case 'name':
          content.push(
            <Text id={ClientsForm.FieldMetaName} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;

        case 'surname':
          content.push(
            <Text id={ClientsForm.FieldMetaSurname} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;

        case 'address':
          content.push(
            <Text id={ClientsForm.FieldMetaAddress} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;

        case 'email':
          content.push(
            <Text id={ClientsForm.FieldMetaEmail} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;

        case 'phone':
          content.push(
            <Text id={ClientsForm.FieldMetaPhone} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;

        default:
          content.push(
            <Text id={ClientsForm.FieldMetaUnknownField} fields={{key: k}} />,
            <Inputs.Input autocomplete='off' width={'1-1'} blank margin='small-bottom'
              { ...this.store.embededFieldMeta(['meta', k]) } />,
          );

          break;
      }

      return {
        content,
      };
    }) as IItem[];
  }
}
