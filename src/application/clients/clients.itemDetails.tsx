import { Component, h } from 'preact';

import { ICustomFormActionTraps } from '../../core/forms/form.custom';
import { I18nStore } from '../../core/intl';
import { Route } from '../../core/routing';
import { computed, Containers, Headings } from '../../core/ui/src';
import { deepClone } from '../../core/util';
import { ISectionStorage } from '../main/section/section.layout';
import { IClient } from './client.type';
import { ClientsForm } from './clients.form';
import { UpdateClientsStore } from './clients.form.update.store';

interface IClientsDetailsProps {
  storage: ISectionStorage<IClient>;
  i18n: I18nStore;
  route: Route;
  traps?: ICustomFormActionTraps;
}

export class ClientsDetails extends Component<IClientsDetailsProps, any> {
  public render() {
    const item = this.item;

    return <Containers.Div overflow='auto' width='1-1' animation={['fade']}>
      <Headings.H3 text={['top', 'emphasis']}
        padding='remove' margin='top' divider>
        { item.identifier }
      </Headings.H3>
      <ClientsForm layout='horizontal' action='update'
        overwrite={new UpdateClientsStore(this.item)}
        actionTraps={this.props.traps || {}} />
    </Containers.Div>;
  }

  @computed
  private get item() {
    return deepClone(this.props.storage.item);
  }
}
