import { action } from '../../core/store';
import { IClient } from './client.type';
import { ClientsStore } from './clients.form.store';

export class UpdateClientsStore extends ClientsStore {
  @action
  public resetFields() {
    super.resetFields();

    if (this.initialValues) {
      (Object.keys(this.initialValues) as Array<keyof IClient>).forEach((k) => {
        (this.values as any)[k].value = (this.initialValues as any)[k];
      });
    }
  }
}
