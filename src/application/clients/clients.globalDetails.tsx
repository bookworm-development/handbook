import { Component, h } from 'preact';

// import { HighlightList } from '../../core/highlight-list';
import { I18nStore, Text } from '../../core/intl';
import { Route } from '../../core/routing';
import { observer } from '../../core/store';
import { Containers, Headings } from '../../core/ui/src';
// import { Charts, someColors } from '../main/charts';
import { ISectionStorage } from '../main/section/section.layout';
import { IClient } from './client.type';

interface IClientsGlobalDetailsProps {
  storage: ISectionStorage<IClient>;
  i18n: I18nStore;
  route: Route;
}

@observer
export class ClientsGlobalDetails extends Component<IClientsGlobalDetailsProps, any> {
  private static DetailsLabel = 'clients.details.label';

  public render() {
    return <Containers.Div animation={['fade']}>
      <Headings.H3 text={['uppercase', 'muted']}
        padding='remove' margin='top' divider>
          <Text id={ClientsGlobalDetails.DetailsLabel}
            fields={{ label: this.props.i18n.translate('routes.rights.plural') }} />
        </Headings.H3>
      {/* <HighlightList options={{ width: 'child-1-4' }} items={
          codeFamilies.map((f) => ({
            details: <Containers.Div userSelect='none'>{f}</Containers.Div>,
            main: <Containers.Div text='muted' userSelect='none'>{coverages[f]}</Containers.Div>,
          }))
        }/> */}

      <hr />

      {/* <Containers.Div padding={['large', 'remove-top', 'remove-bottom']}>
        <Charts.Pie data={this.attachedToProductsChartData} width={400} height={400} options={{
            animation: { animateRotate: true, animateScale: true },
            legend: { display: true, position: 'bottom' },
            title: {
              display: true,
              position: 'bottom',
              text: this.props.i18n.translate(ClientsGlobalDetails.AttachedChartTitle),
            },
          }} />
      </Containers.Div>

      <Containers.Div padding={['large', 'remove-top', 'remove-bottom']} margin='remove'>
        <Charts.Pie data={this.productsByCodeFamilyChartData} width={400} height={400} options={{
            animation: { animateRotate: true, animateScale: true },
            legend: { display: true, position: 'bottom' },
            title: {
              display: true,
              position: 'bottom',
              text: this.props.i18n.translate(ClientsGlobalDetails.ByProductChartTitle),
            },
          }} />
      </Containers.Div> */}

    </Containers.Div>;
  }

  // @computed
  // private get coverages() {
  //   const coverage = codeFamilies.reduce((ret, f) => {
  //     ret[f] = 0;

  //     return ret;
  //   }, {} as { [family in BarCodes]: number; });

  //   this.props.storage.list.forEach((barCode) => {
  //     const family = this.barCodeFamilyLookup[barCode.type] || this.determineFamily(barCode.type);
  //     coverage[family] += 1;
  //   });

  //   return coverage;
  // }

  // private determineFamily(type: BarCodes) {
  //   if (!this.barCodeFamilyLookup[type]) {
  //     for (const f of codeFamilies) {
  //       if (type.indexOf(f) > -1) {
  //         this.barCodeFamilyLookup[type] = f;
  //         break;
  //       }
  //     }
  //   }

  //   return this.barCodeFamilyLookup[type] as BarCodes;
  // }

  // @computed
  // private get attachedToProductsChartData() {
  //   const barCodes = this.props.storage.list;
  //   const products = this.props.products;

  //   const data: number[] = [products.length, barCodes.length - products.length];
  //   const colors: string[] = someColors(data.length);
  //   const labels: string[] = [
  //     this.props.i18n.translate('clients.details.chart.attached'),
  //     this.props.i18n.translate('clients.details.chart.notAttached'),
  //   ];

  //   return {
  //     datasets: [
  //       {
  //         backgroundColor: colors,
  //         borderColor:
  //           new Array(data.length).fill('#222222'),
  //         borderWidth: 3,
  //         data,
  //       },
  //     ],
  //     labels,
  //   };
  // }

  // @computed
  // private get productsByCodeFamilyChartData() {
  //   const products = this.props.products;
  //   const barCodes = this.props.storage.list.reduce((ret, b) => {
  //     ret[b.id] = b;

  //     return ret;
  //   }, {} as { [id: string]: IBarCode });
  //   const coverage = codeFamilies.reduce((ret, f) => {
  //     ret[f] = 0;

  //     return ret;
  //   }, {} as { [family in BarCodes]: number; });

  //   products.forEach((p) => {
  //     coverage[this.determineFamily(barCodes[p.barCode.id].type)] += 1;
  //   });

  //   const families = (Object.keys(coverage) as BarCodes[]).filter((k) => coverage[k]);
  //   const data: number[] = families.map((k) => coverage[k]);
  //   const colors: string[] = someColors(data.length + 2).slice(2);
  //   const labels: string[] = families.map((f) => f.toUpperCase());

  //   return {
  //     datasets: [
  //       {
  //         backgroundColor: colors,
  //         borderColor:
  //           new Array(data.length).fill('#222222'),
  //         borderWidth: 3,
  //         data,
  //       },
  //     ],
  //     labels,
  //   };
  // }
}
