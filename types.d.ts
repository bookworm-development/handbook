declare module '*.jpeg';
declare module '*.jpg';
declare module '*.gif';
declare module '*.png';
declare module '*.svg';

declare function Classify<T, S>(config: { get: () => any; set: (value: any) => void; }): (new () => S);
