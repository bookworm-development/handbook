import { Component } from "preact";

declare module 'mobx-preact' {
  type ComponentErrorHdl = (error: Error) => void;

  // Allows handling of component level mobx errors
  export function onError(handler: ComponentErrorHdl): void;

  // Allows strict mode status modifications
  export function useStrict(strictMode: boolean): void;

  export function inject(param1: string[] | ((allStores: { [p: string]: {} }) => {})): void;

  export function observer(): void;

  export function Provider(): Component;
}
