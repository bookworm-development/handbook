const { CSSPlugin, CSSResourcePlugin, FuseBox, ImageBase64Plugin, JSONPlugin, SassPlugin, WebIndexPlugin } = require("fuse-box");
const { AggregateLocalesPlugin } = require('../src/core/fuse-box-aggregate-locales/dist/src');
const { ContextualTransformer } = require('../src/core/typescript-contextual-transformer/dist/src');
const path = require('path');

const fuse = FuseBox.init({
  cache: true,
  homeDir: "../src",
  target: "browser@es6",
  output: "../dist/$name.js",
  modulesFolder: './modules',
  transformers: {
    before: [
      // ContextualTransformer({
      //   logging: { file: 'random_logging.txt', level: 'Debug' },
      //   plugins: [
      //     {
      //       name: 'InterfaceAnalysis',
      //       options: {
      //         disallowDuplicateNames: false,
      //         deepDuplicateAnalysis: true,
      //       },
      //     },
      //     {
      //       name: 'ClassifyTransformation',
      //       options: {
      //         triggerName: 'Classify'
      //       },
      //     }
      //   ],
      // }),
    ]
  },
  plugins: [
    ImageBase64Plugin(),
    JSONPlugin(),
    [
      SassPlugin(),
      CSSResourcePlugin({ dist: '../dist/css', inline: true }),
      CSSPlugin(),
    ],
    WebIndexPlugin({
      appendBundles: true,
      async: true,
      author: 'Ghepes Doru (Bookworm Development SRL)',
      charset: 'UTF-8',
      description: 'Handbook replacement software',
      title: 'Handbook',
      template: 'index.template.html',
    }),
    AggregateLocalesPlugin({
      locales: ['en', 'ro'],
    }),
  ],
});

fuse.dev(); // launch http server

fuse
  .bundle("app")
  .instructions(" > index.tsx")
  .hmr()
  .watch();
fuse.run();
